<?php
// dd($stockSales);


?>



@extends('layouts.app')

@include('stock_sale.menu')

@section('content')

@if($stockSales->count() != 0)
    <div class="row">


        <div class="card-box">

            <h4 class="header-title m-t-0 m-b-30">stock_sales</h4>

            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <!-- <div class="panel-heading">
                                                                                            <h4>Invoice</h4>
                                                                                        </div> -->
                        <div class="panel-body">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h2 class="">Stock Sales -
                                        <span class="text-danger">Ataco reports</span>
                                        </h3>
                                </div>
                                <div class="pull-right">
                                    ...
                                </div>

                            </div>
                            <hr>
                            <div>
                                {{--  <pre>  --}}
                                    {{$stockSales->links()}}
                                        <table class="table table-striped m-0">
                                                <thead>
                                                    <tr>
                                                        <th>Ataco_code</th>
                                                        <th>Code</th>
                                                        <th>Opis</th>
                                                        <th>Prev_total_stock</th>
                                                        <th>Shipment</th>
                                                        <th>SBrijeg</th>
                                                        <th>Bihac</th>
                                                        <th>Tuzla</th>
                                                        <th>Sarajevo</th>
                                                        <th>Laktasi</th>
                                                        <th>Total_stock</th>
                                                        <th>Type</th>
                                                        <th>Error</th>
                                                        <th>Mjesec</th>
                                                        <th>Godina</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @foreach ($stockSales as $s)
                                                        {{-- @foreach ($item as $s) --}}

                                                        <tr>
                                                            <td>{{$s->ataco_code}}</td>
                                                            <td>{{$s->code}}</td>
                                                            <td>{{str_limit($s->opis, 19)}}</td>
                                                            <td>{{$s->prev_total_stock}}</td>
                                                            <td>{{$s->shipment}}</td>
                                                            <td>{{$s->sbrijeg}}</td>
                                                            <td>{{$s->bihac}}</td>
                                                            <td>{{$s->tuzla}}</td>
                                                            <td>{{$s->sarajevo}}</td>
                                                            <td>{{$s->laktasi}}</td>
                                                            <td>{{$s->total_stock}}</td>
                                                            <td>{{$s->type}}</td>
                                                            <td><span class="btn-small btn-danger">{{$s->error}}</span></td>
                                                            <td>{{$s->month}}</td>
                                                            <td>{{$s->year}}
                                                                    <!-- <a href="edit.html" class="">edit</a>
                                                                        <a href="#" class="">disable</a> -->
                                                                    </td>
                                                                </tr>
                                                        {{-- @endforeach --}}
                                                    @endforeach

                                                </tbody>
                                            </table>

                                {{--  @foreach ($stockSales as $item)
                                    {{--  {{print_r($item->toArray())}}  --}}
                                    {{--  {{ $item->ataco_code }}         |  --}}
                                    {{--  {{ $item->kat_num }}            |  --}}
                                    {{--  {{ $item->opis }}               |  --}}
                                    {{--  {{ $item->prev_total_stock }}   |  --}}
                                    {{--  {{ $item->shipment }}           |  --}}
                                    {{--  {{ $item->sbrijeg }}            |  --}}
                                    {{--  {{ $item->bihac }}              |  --}}
                                    {{--  {{ $item->tuzla }}              |  --}}
                                    {{--  {{ $item->sarajevo }}           |  --}}
                                    {{--  {{ $item->laktasi }}            |  --}}
                                    {{--  {{ $item->total_stock }}        |  --}}
                                    {{--  {{ $item->type }}               |  --}}
                                    {{--  {{ $item->date }}               |  --}}
                                    {{--  {{ $item->code }}               |  --}}
                                    {{--  {{ $item->product_id }}         |  --}}
                                    {{--  {{ $item->acode_date_type }}    |  --}}
                                    {{--  {{ $item->month }}              |  --}}
                                    {{--  {{ $item->year }}               |  --}}
                                    {{--  <span class="btn-small btn-danger">{{ $item->error }}</span>              |  --}}
                                    {{--  {{ $item->average }}            |  --}}
                                    {{--  {{ $item->created_at }}         |
                                    {{ $item->updated_at }}         |  --}}
                                    <hr>
                                {{--  @endforeach   --}}
                            {{--  </pre>  --}}
                            </div>

                            <!-- end row -->

                            <div class="hidden-print">
                                <div class="pull-right">
                                    <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light">
                                        <i class="fa fa-print"></i>
                                    </a>
                                    <a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

@endif

@if($stockSales->count()==0)
    <h3>Nema zapisa!</h3>
@endif

@endsection