@extends('layouts.app')

@include('stock_sale.menu')

@section('content')

<div class="row">

    <div class="card-box">

        <h4 class="header-title m-t-0 m-b-30">Insert Ataco stock_sales</h4>

        <hr>

        <div class="row">
            <form action="/stocksales/createfromtext" method="POST">
                {{ csrf_field() }}
                <div class="col-md-12">
                        <div class="form-group">
                                {{--  <label class="col-md-2 control-label">Text area</label>  --}}
                                <div class="col-md-12">
                                    <textarea class="form-control" rows="5" cols="11" name="text" placeholder="Insert Ataco stock_sales koristeci csv formatirani text.  Iskopirajte stock ili sales pravo iz excela"></textarea>
                                </div>
                            </div>

            </div>

            <div class="col-md-12">

                    <h3>Info:</h3>
                    vrlo vazan je prvi red jer definise imena kolona u aplikaciji:
                    <pre>
Redni<i class="zmdi zmdi-long-arrow-tab text-danger"></i>kat_num<i class="zmdi zmdi-long-arrow-tab text-danger"></i>ataco_code<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Opis<i class="zmdi zmdi-long-arrow-tab text-danger"></i>prev_total_stock<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Shipment<i class="zmdi zmdi-long-arrow-tab text-danger"></i>S.Brijeg<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Bihac<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Tuzla<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Sarajevo<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Laktasi<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Total_stock<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Date<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Type
<br>
Redni	kat_num	ataco_code	Opis	prev_total_stock	Shipment	S.Brijeg	Bihac	Tuzla	Sarajevo	Laktasi	Total_stock	Date	Type

</pre>

                    Primjer texta za unos stock and sales-a:
                    <pre>
Redni<i class="zmdi zmdi-long-arrow-tab text-danger"></i>kat_num<i class="zmdi zmdi-long-arrow-tab text-danger"></i>ataco_code<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Opis<i class="zmdi zmdi-long-arrow-tab text-danger"></i>prev_total_stock<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Shipment<i class="zmdi zmdi-long-arrow-tab text-danger"></i>S.Brijeg<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Bihac<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Tuzla<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Sarajevo<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Laktasi<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Total_stock<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Date<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Type
10<i class="zmdi zmdi-long-arrow-tab text-danger"></i>41000 <i class="zmdi zmdi-long-arrow-tab text-danger"></i>10001051<i class="zmdi zmdi-long-arrow-tab text-danger"></i>HIPP 1 ORGANIC 300G<i class="zmdi zmdi-long-arrow-tab text-danger"></i>943<i class="zmdi zmdi-long-arrow-tab text-danger"></i>0<i class="zmdi zmdi-long-arrow-tab text-danger"></i>203<i class="zmdi zmdi-long-arrow-tab text-danger"></i>54<i class="zmdi zmdi-long-arrow-tab text-danger"></i>60<i class="zmdi zmdi-long-arrow-tab text-danger"></i>106<i class="zmdi zmdi-long-arrow-tab text-danger"></i>128<i class="zmdi zmdi-long-arrow-tab text-danger"></i>551<i class="zmdi zmdi-long-arrow-tab text-danger"></i>2017-06-11<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Stock
12<i class="zmdi zmdi-long-arrow-tab text-danger"></i>41000 <i class="zmdi zmdi-long-arrow-tab text-danger"></i>10001061<i class="zmdi zmdi-long-arrow-tab text-danger"></i>HIPP 1 ORGANIC 800G<i class="zmdi zmdi-long-arrow-tab text-danger"></i>914<i class="zmdi zmdi-long-arrow-tab text-danger"></i>0<i class="zmdi zmdi-long-arrow-tab text-danger"></i>265<i class="zmdi zmdi-long-arrow-tab text-danger"></i>75<i class="zmdi zmdi-long-arrow-tab text-danger"></i>48<i class="zmdi zmdi-long-arrow-tab text-danger"></i>139<i class="zmdi zmdi-long-arrow-tab text-danger"></i>221<i class="zmdi zmdi-long-arrow-tab text-danger"></i>748<i class="zmdi zmdi-long-arrow-tab text-danger"></i>2017-06-11<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Stock
14<i class="zmdi zmdi-long-arrow-tab text-danger"></i>41000 <i class="zmdi zmdi-long-arrow-tab text-danger"></i>10001071<i class="zmdi zmdi-long-arrow-tab text-danger"></i>HIPP 1 COMBIOTIC INFANT 300G<i class="zmdi zmdi-long-arrow-tab text-danger"></i>2,050<i class="zmdi zmdi-long-arrow-tab text-danger"></i>0<i class="zmdi zmdi-long-arrow-tab text-danger"></i>458<i class="zmdi zmdi-long-arrow-tab text-danger"></i>55<i class="zmdi zmdi-long-arrow-tab text-danger"></i>118<i class="zmdi zmdi-long-arrow-tab text-danger"></i>165<i class="zmdi zmdi-long-arrow-tab text-danger"></i>422<i class="zmdi zmdi-long-arrow-tab text-danger"></i>1,218<i class="zmdi zmdi-long-arrow-tab text-danger"></i>2017-06-11<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Stock
                    </pre>

                <div class="col-md-12 m-t-30">
                    <button type="submit " class="btn btn-purple waves-effect waves-light ">Submit</button>
                </div>
            </div>

            </form>
        </div>


    </div>

</div>


@endsection