<?php


?>



@extends('layouts.app')

@section('sidebar')
    @parent
    <hr>
    @component('layouts.link',['link_text' => 'Stock Sales create', 'link'=>'/stocksales/create'])
    @endcomponent

@endsection

@section('content')

@if($stockSales->count() != 0)
    <div class="row">
        @php
            // dd($stockSales->count());
            $stockSalesParsed = $stockSales->groupBy('month')->first()->map(function ($item, $key)
                {
                    $regions = collect($item->only('sbrijeg', 'bihac', 'tuzla', 'sarajevo', 'laktasi'));
                    $avgValue = $regions->sum()/$regions->count();
                    $item['avgPcs'] =round($avgValue);
                    $item['errorTotal'] = ($regions->sum() != $item['total_stock'] ) ? 1 : 0 ;
                    // dd($item);
                    return $item;

                });
                $stockSalesYear = $stockSalesParsed->groupBy('year');
                $stockSalesmonth = $stockSalesParsed->sortByDesc('total_stock')->groupBy('month');
        @endphp

        <div class="card-box">

            <h4 class="header-title m-t-0 m-b-30">stock_sales</h4>

            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <!-- <div class="panel-heading">
                                                                                            <h4>Invoice</h4>
                                                                                        </div> -->
                        <div class="panel-body">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h2 class="">Stock Sales -
                                        <span class="text-danger">Ataco reports</span>
                                        </h3>
                                </div>
                                <div class="pull-right">
                                    ...
                                </div>

                        </div>
                            <hr>

                            <h3>Godina: {{$stockSalesYear->keys()[0]}}</h3>
                            @php $stocksaleMonthPeriods = $stockSalesmonth->first()->split(3) @endphp
                            <h4>Mjesec: {{$stockSalesmonth->keys()[0]}}</h3>
                                @foreach ($stocksaleMonthPeriods as $months)
                                {{--  {{dd($stockSalesmonth)}}  --}}

                                    <div class="col-md-6">
                                    @foreach ($months as $month)

                                    @if ($month['total_stock']==0)
                                        @continue
                                    @endif

                                    @php $class="bg-primary text-uppercase m-r-5 m-l-5"  @endphp
                                    @if ($month['errorTotal']==1)
                                        @php $class=""  @endphp
                                    @endif


                                    <div class="@if($month['errorTotal'] == 1)text-white hader-title bg-danger p-t-10 p-b-10 @endif m-t-10 ">
                                        {{--  @foreach ($month as $item)  --}}

                                                <span class="{{$class}}">{{$loop->index}}  Ataco code: <strong>{{$month['ataco_code']}}</strong> </span>
                                                <span class="{{$class}}"> {{$month['type']}} </span> |
                                                Sred. vrijednost(regije):{{$month['avgPcs']}} - Total:{{$month['total_stock']}} <br>

                                        {{--  @endforeach  --}}
                                    </div>
                                    @endforeach
                                </div>
                                @endforeach

                                <hr>
                            {{--  <table class="table table-striped m-0">
                                <thead>
                                    <tr>
                                        <th>Ataco_code</th>
                                        <th>Code</th>
                                        <th>Opis</th>
                                        <th>Prev_total_stock</th>
                                        <th>Shipment</th>
                                        <th>SBrijeg</th>
                                        <th>Bihac</th>
                                        <th>Tuzla</th>
                                        <th>Sarajevo</th>
                                        <th>Laktasi</th>
                                        <th>Total_stock</th>
                                        <th>Type</th>
                                        <th>Mjesec</th>
                                        <th>Godina</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($stockSales as $s)
                                    <tr>
                                        <td>{{$s->ataco_code}}</td>
                                        <td>{{$s->code}}</td>
                                        <td>{{$s->opis}}</td>
                                        <td>{{$s->prev_total_stock}}</td>
                                        <td>{{$s->shipment}}</td>
                                        <td>{{$s->sBrijeg}}</td>
                                        <td>{{$s->bihac}}</td>
                                        <td>{{$s->tuzla}}</td>
                                        <td>{{$s->sarajevo}}</td>
                                        <td>{{$s->laktasi}}</td>
                                        <td>{{$s->total_stock}}</td>
                                        <td>{{$s->type}}</td>
                                        <td>{{$s->month}}</td>
                                        <td>{{$s->year}}
                                            <!-- <a href="edit.html" class="">edit</a>
                                            <a href="#" class="">disable</a> -->
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>  --}}
                            <br>
                            <br>
                            <br>
                            <hr>
                            <!-- end row -->

                            <div class="hidden-print">
                                <div class="pull-right">
                                    <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light">
                                        <i class="fa fa-print"></i>
                                    </a>
                                    <a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

@endif

@if($stockSales->count()==0)
    <h3>Nema zapisa!</h3>
@endif

@endsection