                                        <table class="table table-striped m-0">
                                                <thead>
                                                    <tr>
                                                        <th>Ataco_code</th>
                                                        <th>Code</th>
                                                        <th>Opis</th>
                                                        <th>Prev_total_stock</th>
                                                        {{--  <th>Shipment</th>
                                                        <th>SBrijeg</th>
                                                        <th>Bihac</th>
                                                        <th>Tuzla</th>
                                                        <th>Sarajevo</th>
                                                        <th>Laktasi</th>  --}}
                                                        <th>Total_stock</th>
                                                        <th>Type</th>
                                                        <th>Error</th>
                                                        <th>Mjesec</th>
                                                        <th>Godina</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @foreach ($stockSales as $s)
                                                    <tr>
                                                        <td>{{$s->ataco_code}}</td>
                                                        <td>{{$s->code}}</td>
                                                        <td>{{$s->opis}}</td>
                                                        <td>{{$s->prev_total_stock}}</td>
                                                        {{--  <td>{{$s->shipment}}</td>
                                                        <td>{{$s->sBrijeg}}</td>
                                                        <td>{{$s->bihac}}</td>
                                                        <td>{{$s->tuzla}}</td>
                                                        <td>{{$s->sarajevo}}</td>
                                                        <td>{{$s->laktasi}}</td>  --}}
                                                        <td>{{$s->total_stock}}</td>
                                                        <td>{{$s->type}}</td>
                                                        <td><span class="btn-small btn-danger">{{$s->error}}</span></td>
                                                        <td>{{$s->month}}</td>
                                                        <td>{{$s->year}}
                                                            <!-- <a href="edit.html" class="">edit</a>
                                                            <a href="#" class="">disable</a> -->
                                                        </td>
                                                    </tr>
                                                    @endforeach

                                                </tbody>
                                            </table>