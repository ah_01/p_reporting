<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    {{--  TODO:Remove DevX view and files  --}}
    <title>DevExtreme Demo</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <script src="/devx/js/jquery-3.1.0.min.js"></script>
    <script src="/devx/js/cldr.min.js"></script>
    <script src="/devx/js/cldr/event.min.js"></script>
    <script src="/devx/js/cldr/supplemental.min.js"></script>
    <script src="/devx/js/cldr/unresolved.min.js"></script>
    <script type="text/javascript" src="/devx/js/globalize.min.js"></script>
    <script type="text/javascript" src="/devx/js/globalize/message.min.js"></script>
    <script type="text/javascript" src="/devx/js/globalize/number.min.js"></script>
    <script type="text/javascript" src="/devx/js/globalize/currency.min.js"></script>
    <script type="text/javascript" src="/devx/js/globalize/date.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/devx/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="/devx/css/dx.common.css" />
    <link rel="dx-theme" data-theme="generic.light" href="/devx/css/dx.light.css" />
    <script src="/devx/js/dx.all.js"></script>
    <link rel="stylesheet" type ="text/css" href ="styles.css" />
</head>
<body class="dx-viewport">
    <div class="demo-container">
        <div id="pivotgrid-demo">
            <div id="pivotgrid-chart"></div>
            <div id="pivotgrid"></div>
        </div>
    </div>

    <script>
            $(function(){

                var json = (function () {
                    var json = null;
                    $.ajax({
                        'async': false,
                        'global': false,
                        'url': "stocksales.json",
                        'dataType': "json",
                        'success': function (data) {
                            json = data;
                        }
                    });
                    return json;
                })();
                console.log(json);
                var pivotGridChart = $("#pivotgrid-chart").dxChart({
                    commonSeriesSettings: {
                        type: "bar"
                    },

                    size: {
                        height: 320
                    },
                    adaptiveLayout: {
                        width: 450
                    }
                }).dxChart("instance");

                var pivotGrid = $("#pivotgrid").dxPivotGrid({
                    allowSortingBySummary: true,
                    allowFiltering: true,
                    showBorders: true,
                    showColumnGrandTotals: false,
                    showRowGrandTotals: false,
                    showRowTotals: false,
                    showColumnTotals: false,
                    fieldChooser: {
                        enabled: true
                    },
                    dataSource: json,

                }).dxPivotGrid("instance");

                pivotGrid.bindChart(pivotGridChart, {
                    dataFieldsDisplayMode: "splitPanes",
                    alternateDataFields: false
                });
            });



</script>
</body>
</html>