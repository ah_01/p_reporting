@section('sidebar')
    @parent
    <hr>

    @component('layouts.link',['link_text' => 'StockSales all', 'link'=>'/stocksales/all'])
    @endcomponent

    @component('layouts.link',['link_text' => 'StockSales search', 'link'=>'/stocksales/scan'])
    @endcomponent

    @component('layouts.link',['link_text' => 'StockSales create', 'link'=>'/stocksales/create'])
    @endcomponent


@endsection