
@extends('layouts.app')

@include('stock_sale.menu')

@section('content')

@if($stockSales->count() != 0)
{{-- {{dd($stockSales)}} --}}
{{-- @php dd(($stockSales instanceof Illuminate\Pagination\LengthAwarePaginator)) @endphp --}}
    <div class="row">


        <div class="card-box">

            <h4 class="header-title m-t-0 m-b-30">stock_sales</h4>
            <div class="row col-md-12">
                    <form action="/" method="get">
                        {{ csrf_field() }}
                        <input type="hidden" name="q" value="q">

                        <div class="col-md-2 form-group">
                            <label class="control-label">Code / Ataco Code</label>
                        <input type="text" class="form-control" name="code" value="{{request('code')}}">
                        </div>

                        <div class="col-md-1 form-group">
                            <label class="control-label">date</label>
                            <input type="text" class="form-control" name="date" value="{{request('date')}}">
                        </div>

                        <div class="col-md-1 form-group">
                            <label class="control-label">month</label>
                            <input type="text" class="form-control" name="month" value="{{request('month')}}">
                        </div>

                        <div class="col-md-1 form-group">
                            <label class="control-label">year</label>
                            <input type="text" class="form-control" name="year" value="{{request('year')}}">
                        </div>

                        <div class="col-md-2 form-group">
                            <label class="control-label">type</label>
                            <input type="text" class="form-control" name="type" value="{{request('type')}}">
                        </div>

                        <div class="col-md-2 form-group">
                            <label class="control-label">error</label>
                            <input type="text" class="form-control" name="error" value="{{request('error')}}">
                        </div>


                        <div class="col-md-2">
                                <button type="submit " class="btn btn-purple waves-effect waves-light ">Submit</button>
                                <br>
                                @if (request('q'))
                                    <a href="{{url()->full()}}&excel=1">Export Excel</a>
                                @endif
                        </div>

                    </form>
                    <div class="col-md-12">
                            {{$stockSales->count()}} stavki nadjeno
                    </div>
                    <br>
                    <br>
                    <br>
                    <hr>


            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <!-- <div class="panel-heading">
                                                                                            <h4>Invoice</h4>
                                                                                        </div> -->
                        <div class="panel-body">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h2 class="">Stock Sales -
                                        <span class="text-danger">Ataco reports</span>
                                        </h3>
                                </div>
                                <div class="pull-right">
                                    ...
                                </div>

                            </div>
                            <hr>
                            <div>
                            {{--  <pre>  --}}
                                @if (method_exists($stockSales, 'links'))
                                    {{$stockSales->appends(request()->all())->links()}}
                                @endif
                                        <table class="table table-striped m-0">
                                                <thead>
                                                    <tr>
                                                        <th>Ataco_code</th>
                                                        <th>Code</th>
                                                        <th>Opis</th>
                                                        <th>date</th>
                                                        <th>Prev_total_stock</th>
                                                        <th>Shipment</th>
                                                        <th>SBrijeg</th>
                                                        <th>Bihac</th>
                                                        <th>Tuzla</th>
                                                        <th>Sarajevo</th>
                                                        <th>Laktasi</th>
                                                        <th>Total_stock</th>
                                                        <th>average</th>
                                                        <th>Type</th>
                                                        <th>Error</th>
                                                        <th>Mjesec</th>
                                                        <th>Godina</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @foreach ($stockSales as $s)
                                                    <tr>
                                                        <td>{{$s->ataco_code}}</td>
                                                        <td>{{$s->code}}</td>
                                                        <td>{{$s->opis}}</td>
                                                        <td>{{$s->date}}</td>
                                                        <td>{{$s->prev_total_stock}}</td>
                                                        <td>{{$s->shipment}}</td>
                                                        <td>{{$s->sBrijeg}}</td>
                                                        <td>{{$s->bihac}}</td>
                                                        <td>{{$s->tuzla}}</td>
                                                        <td>{{$s->sarajevo}}</td>
                                                        <td>{{$s->laktasi}}</td>
                                                        <td>{{$s->total_stock}}</td>
                                                        <td>{{$s->average}}</td>
                                                        <td>{{$s->type}}</td>
                                                        <td><span class="btn-small btn-danger">{{$s->error}}</span></td>
                                                        <td>{{$s->month}}</td>
                                                        <td>{{$s->year}}
                                                            <!-- <a href="edit.html" class="">edit</a>
                                                            <a href="#" class="">disable</a> -->
                                                        </td>
                                                    </tr>
                                                    @endforeach

                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>{{$s->shipment}}</td>
                                                        <td>{{$s->sBrijeg}}</td>
                                                        <td>{{$s->bihac}}</td>
                                                        <td>{{$s->tuzla}}</td>
                                                        <td>{{$s->sarajevo}}</td>
                                                        <td>{{$s->laktasi}}</td>
                                                        <td>{{$stockSales->sum('total_stock')}}</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <!-- <a href="edit.html" class="">edit</a>
                                                            <a href="#" class="">disable</a> -->
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>

                                        @if (method_exists($stockSales, 'links'))
                                            {{$stockSales->appends(request()->all())->links()}}
                                        @endif

                                {{--  @foreach ($stockSales as $item)
                                    {{--  {{print_r($item->toArray())}}  --}}
                                    {{--  {{ $item->ataco_code }}         |  --}}
                                    {{--  {{ $item->kat_num }}            |  --}}
                                    {{--  {{ $item->opis }}               |  --}}
                                    {{--  {{ $item->prev_total_stock }}   |  --}}
                                    {{--  {{ $item->shipment }}           |  --}}
                                    {{--  {{ $item->sbrijeg }}            |  --}}
                                    {{--  {{ $item->bihac }}              |  --}}
                                    {{--  {{ $item->tuzla }}              |  --}}
                                    {{--  {{ $item->sarajevo }}           |  --}}
                                    {{--  {{ $item->laktasi }}            |  --}}
                                    {{--  {{ $item->total_stock }}        |  --}}
                                    {{--  {{ $item->type }}               |  --}}
                                    {{--  {{ $item->date }}               |  --}}
                                    {{--  {{ $item->code }}               |  --}}
                                    {{--  {{ $item->product_id }}         |  --}}
                                    {{--  {{ $item->acode_date_type }}    |  --}}
                                    {{--  {{ $item->month }}              |  --}}
                                    {{--  {{ $item->year }}               |  --}}
                                    {{--  <span class="btn-small btn-danger">{{ $item->error }}</span>              |  --}}
                                    {{--  {{ $item->average }}            |  --}}
                                    {{--  {{ $item->created_at }}         |
                                    {{ $item->updated_at }}         |  --}}
                                    <hr>
                                {{--  @endforeach   --}}
                            {{--  </pre>  --}}
                            </div>

                            <!-- end row -->

                            <div class="hidden-print">
                                <div class="pull-right">
                                    <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light">
                                        <i class="fa fa-print"></i>
                                    </a>
                                    <a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

@endif

@if($stockSales->count()==0)
    <h3>Nema zapisa!</h3>
@endif

@endsection