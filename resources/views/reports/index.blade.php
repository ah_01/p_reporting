
@extends('layouts.appa')


@section('sidebar')
    @parent

    @component('layouts.link',['link_text' => 'Invoices', 'link'=>'/invoice'])
    @endcomponent


    @component('layouts.link',['link_text' => 'Invoices-dodaj', 'link'=>'/invoice/create'])
    @endcomponent

@endsection


@section('content')
<div class="row">
        {{-- {{dd($sales_month)}} --}}
        <div class="panel panel-default">
        <!-- <div class="panel-heading">
                                            <h4>Invoice</h4>
                                        </div> -->
        <div class="panel-body">

            <div class="row">
                Choose a year:
                <form method="get" action="/reports">
                    {{ csrf_field() }}
                    <div class="col-md-2">
                        <select class="form-control" name="year">
                            @foreach ($years as $key=>$year)
                                 <option>{{$year}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <button type="submit " class="btn btn-purple waves-effect waves-light ">Submit</button>
                    </div>
                    <div class="col-md-4"></div>
                </form>
            </div>
            <hr>
            <!-- end row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">

                        <h3>Quick overview {{$currentYear}}</h3>
                        {{-- {{dump($data['stock'])}}
                        {{dump($data['sale'])}} --}}
                        <div class="col-md-3">
                                Stock: {{$data['stock']}}
                                <br>
                                <br>
                                <hr>
                        </div>
                        <div class="col-md-3">
                                Sales: {{$data['sale']}}<br>
                                Otpis: {{$data['otpis']}}<br>
                                <hr>
                                @php
                                    $sales_otpis = $data['sale'] + $data['otpis'];
                                @endphp
                                Sales + otpis = <strong>{{$sales_otpis}}</strong>
                        </div>
                        <div class="col-md-3">
                            Diff Stock vs Sales + Otpis = <strong>{{$data['stock'] - $sales_otpis}}</strong>
                            <br>
                            Reklamni total: {{$data['reklamni']}}
                        </div>
                        <div class="col-md-3">
                            &nbsp;
                        </div>

                        <div class="col-md-12">
                              <hr>
                             <div>{!! $chart->container() !!}</div>

                            {!! $chart->script() !!}

                        </div>

                        <div class="col-md-12">
                            <hr>
                             <div>{!! $stock_sale_months->container() !!}</div>

                            {!! $stock_sale_months->script() !!}
                        </div>

                        <hr>

                        <script src=//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js charset=utf-8></script>
                        <table class="table table-striped m-0">
                                <thead>
                                    <tr>
                                        <th>Months</th>
                                        @foreach ($stock_month as $key=>$item)
                                        <th>{{$key+1}}</th>
                                        @endforeach
                                        {{-- <th>total</th> --}}
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td>Stock</td>
                                        @foreach ($stock_month as $s)
                                        <td>{{$s}}</td>

                                        @endforeach
                                        {{-- <td>{{array_sum($stock_month)}}</td> --}}
                                    </tr>

                                    <tr>
                                        <td>Sales</td>
                                        @foreach ($sales_month as $s)
                                        <td>{{$s}}</td>

                                        @endforeach
                                        {{-- <td>{{array_sum($sales_month)}}</td> --}}
                                    </tr>

                                    <tr>
                                        <td>Otpis</td>
                                        @foreach ($otpis_month as $s)
                                        <td>{{$s}}</td>

                                        @endforeach
                                        {{-- <td>{{array_sum($sales_month)}}</td> --}}
                                    </tr>

                                    <tr>
                                        <td>Reklamni</td>
                                        @foreach ($reklamni_month as $s)
                                        <td>{{$s}}</td>

                                        @endforeach
                                        {{-- <td>{{array_sum($sales_month)}}</td> --}}
                                    </tr>

                                </tbody>
                            </table>


                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6">
                            Initial stock = <strong>{{$initial_stock->sum('total_stock')}}</strong>
                            <br>

                            Current Year Stock: <i>(Initial stock + stockpile) i.e. ATACO Stock</i> = <strong>{{$currentYearStock = $initial_stock->sum('total_stock') + $data['stock']}}</strong>
                            <br>

                            Initial stock for next year <i>(current year stock - sales - otpis)</i> = <strong>{{$currentYearStock - $sales_otpis}}</strong>
                            <br>

                            Actual initial stock for next year: <strong>{{$initial_stock_next}}</strong>
                            <br>
                        </div>
                        <div class="col-md-6">
                            Initial stock + stock - stock for next year - otpis (Actual sales:)
                            <strong>{{$initial_stock->sum('total_stock') + $data['stock'] - $initial_stock_next - $data['otpis']}}</strong>
                            <br>
                            Sales per week: {{round($data['sale']/52, 3)}}
                        </div>
                    </div>

                </div>
            </div>
            <!-- <hr> -->
            <!-- <div class="hidden-print">
                        <div class="pull-right">
                            <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light">
                                <i class="fa fa-print"></i>
                            </a>
                            <a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>
                        </div>
                    </div> -->
        </div>
        </div>
    </div>
@endsection