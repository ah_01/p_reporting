
@extends('layouts.appa')


@section('sidebar')
    @parent

    @component('layouts.link',['link_text' => 'Invoices', 'link'=>'/invoice'])
    @endcomponent


    @component('layouts.link',['link_text' => 'Invoices-dodaj', 'link'=>'/invoice/create'])
    @endcomponent

@endsection


@section('content')
<div class="row">
        {{-- {{dd($sales_month)}} --}}
        <div class="panel panel-default">
        <!-- <div class="panel-heading">
                                            <h4>Invoice</h4>
                                        </div> -->
        <div class="panel-body">

            <div class="row">
                Choose a year:
                <form method="get" action="/reports/running">
                    {{ csrf_field() }}
                    <div class="col-md-2">
                        <select class="form-control" name="year">
                            @foreach ($years as $key=>$year)
                                 <option>{{$year}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <button type="submit " class="btn btn-purple waves-effect waves-light ">Submit</button>
                    </div>
                    <div class="col-md-4"></div>
                </form>
            </div>
            <hr>
            <!-- end row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">

                        <h3>Quick overview {{$currentYear}}</h3>
                        {{-- {{dump($data['stock'])}}
                        {{dump($data['sale'])}} --}}
                        <div class="col-md-3">
                                Stock: {{$data['stock']}}
                                <br>
                                <br>
                                <hr>
                        </div>
                        <div class="col-md-3">
                                Sales: {{$data['sale']}}<br>
                                <hr>

                        </div>
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-3">
                            &nbsp;
                        </div>

                        <script src=//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js charset=utf-8></script>
                        <table class="table table-striped m-0">
                                <thead>
                                    <tr>
                                        <th>Months</th>
                                        @foreach ($stock_month as $key=>$item)
                                        <th>{{$key+1}}</th>
                                        @endforeach
                                        {{-- <th>total</th> --}}
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td>Stock</td>
                                        @foreach ($stock_month as $s)
                                        <td>{{$s}}</td>

                                        @endforeach
                                        {{-- <td>{{array_sum($stock_month)}}</td> --}}
                                    </tr>

                                    <tr>
                                        <td>Sales</td>
                                        @foreach ($sales_month as $s)
                                        <td>{{$s}}</td>

                                        @endforeach
                                        {{-- <td>{{array_sum($sales_month)}}</td> --}}
                                    </tr>


                                </tbody>
                            </table>


                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6">
                            Initial stock = <strong>{{$initial_stock->sum('total_stock')}}</strong>
                            <br>

                            <br>
                            Sales per week: {{round($data['sale']/52, 3)}}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                         <div>{!! $stockpile_running->container() !!}</div>

                        {!! $stockpile_running->script() !!}
                    </div>
                    <script src=//cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js charset=utf-8></script>
                    <hr>
                </div>
            </div>
            <!-- <hr> -->
            <!-- <div class="hidden-print">
                        <div class="pull-right">
                            <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light">
                                <i class="fa fa-print"></i>
                            </a>
                            <a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>
                        </div>
                    </div> -->
        </div>
        </div>
    </div>
@endsection