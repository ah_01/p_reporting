
@extends('layouts.appa')


@section('sidebar')
    @parent

    @component('layouts.link',['link_text' => 'Invoices', 'link'=>'/invoice'])
    @endcomponent


    @component('layouts.link',['link_text' => 'Invoices-dodaj', 'link'=>'/invoice/create'])
    @endcomponent

@endsection


@section('content')
<div class="row">
        {{-- {{dd($sales_month)}} --}}
        <div class="panel panel-default">
        <!-- <div class="panel-heading">
                                            <h4>Invoice</h4>
                                        </div> -->
        <div class="panel-body">

            <div class="row">
                Choose a year:
                <form method="get" action="/reports/pie">
                    {{ csrf_field() }}
                    <div class="col-md-2">
                    <select class="form-control" name="year">
                                <option>2016</option>
                                <option>2017</option>
                                <option>2018</option>
                                <option>2019</option>
                                <option>2020</option>
                        </select>
                        <select class="form-control" name="month">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <button type="submit " class="btn btn-purple waves-effect waves-light ">Submit</button>
                    </div>
                    <div class="col-md-4"></div>
                </form>
            </div>
            <hr>
            <!-- end row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                    @isset($date_keys)
                        @foreach ($date_keys as $key)
                            <a href="{{url()->full()}}&date={{$key}}">{{$key}}</a> <br>
                        @endforeach
                    @endisset

                    @isset($stock_regions)
                        <h3>Quick overview </h3>
                        {{-- {{dump($data['stock'])}}
                        {{dump($data['sale'])}} --}}

                        <div class="col-md-3">
                        </div>
                        <div class="col-md-3">
                            &nbsp;
                        </div>

                        <table class="table table-striped m-0">
                                <thead>
                                    <tr>
                                        <th>Months</th>
                                        <th>sbrijeg</th>
                                        <th>bihac</th>
                                        <th>tuzla</th>
                                        <th>sarajevo</th>
                                        <th>laktasi</th>
                                        <th>total_stock</th>
                                        {{-- <th>total</th> --}}
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td>Stock</td>
                                        @foreach ($stock_regions as $s)
                                        <td>{{$s}}</td>

                                        @endforeach
                                        {{-- <td>{{array_sum($stock_month)}}</td> --}}
                                    </tr>

                                    <tr>
                                        <td>Sales</td>
                                        @foreach ($sales_regions as $s)
                                        <td>{{$s}}</td>

                                        @endforeach
                                        {{-- <td>{{array_sum($sales_month)}}</td> --}}
                                    </tr>


                                </tbody>
                            </table>


                    </div>

                    <div class="col-md-12">
                        Contorl block:

                        Stock total: {{array_sum($stock_regions)}} <br>
                        Sum total: {{array_sum($sales_regions)}} <br>
                        this is unreliable because there are big differences and errors in numbers !!!
                    </div>
            </div>
            <div class="col-md-12">
                    <hr>
                     <div>{!! $pie_stock->container() !!}</div>

                    {!! $pie_stock->script() !!}
                </div>
                <script src=//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js charset=utf-8></script>
                <hr>
            </div>
            <div class="col-md-12">
                    <hr>
                     <div>{!! $pie_sale->container() !!}</div>

                    {!! $pie_sale->script() !!}
                </div>
                <script src=//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js charset=utf-8></script>
                <hr>
            </div>
            <!-- <hr> -->
            <!-- <div class="hidden-print">
                        <div class="pull-right">
                            <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light">
                                <i class="fa fa-print"></i>
                            </a>
                            <a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>
                        </div>
                    </div> -->
        </div>
        </div>
        <script src=//cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js charset=utf-8></script>

        @endisset

    </div>
@endsection