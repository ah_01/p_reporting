
@extends('layouts.app')

@include('stockpile.menu')

@section('content')
{{-- {{dd($stockpile)}} --}}
@if($stockpile->count() != 0)
    <div class="row">

        <div class="card-box">

            <h4 class="header-title m-t-0 m-b-30">stock_sales</h4>

            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <!-- <div class="panel-heading">
                                                                                            <h4>Invoice</h4>
                                                                                        </div> -->
                        <div class="panel-body">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h2 class="">Stockpile
                                        <span class="text-danger">
                                            @isset($total)
                                            {{$stockpile->first()->code}}, Total: {{$total}} pcs.
                                            @endisset
                                        </span>
                                    </h3>
                                </div>
                                <div class="pull-right">
                                    ...
                                </div>

                            </div>
                            <div class="row col-md-12">
                                    <form action="/stockpile" method="get">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="q" value="q">

                                        <div class="col-md-2 form-group">
                                            <label class="control-label">Code</label>
                                        <input type="text" class="form-control" name="code" value="{{request('code')}}">
                                        </div>

                                        <div class="col-md-1 form-group">
                                            <label class="control-label">month</label>
                                            <input type="text" class="form-control" name="month" value="{{request('month')}}">
                                        </div>

                                        <div class="col-md-1 form-group">
                                            <label class="control-label">year</label>
                                            <input type="text" class="form-control" name="year" value="{{request('year')}}">
                                        </div>

                                        <div class="col-md-1 form-group">
                                            <label class="control-label">type</label>
                                            <input type="text" class="form-control" name="type" value="{{request('type')}}">
                                        </div>

                                        <div class="col-md-1 form-group">
                                            <label class="control-label">truck</label>
                                            <input type="text" class="form-control" name="truck" value="{{request('truck')}}">
                                        </div>


                                        <div class="col-md-4">
                                                <button type="submit " class="btn btn-purple waves-effect waves-light ">Submit</button>
                                                <br>
                                                {{-- @if (request('q'))
                                                    <a href="{{url()->full()}}&excel=1">Export Excel</a>
                                                @endif --}}
                                        </div>

                                    </form>
                                    <div class="col-md-12">
                                        @isset($stockpile)
                                            {{$stockpile->count()}} stavki nadjeno
                                        @endisset
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <hr>


                                </div>
                            <hr>
                            <div>
                                {{--  <pre>  --}}

                                        <table class="table table-striped m-0">
                                                <thead>
                                                    <tr>
                                                        {{-- <th>Ataco_code</th> --}}
                                                        <th>Code</th>
                                                        <th>Amount</th>
                                                        <th>Price</th>
                                                        <th>Total</th>
                                                        <th>Date</th>
                                                        <th>Exp Date</th>
                                                        <th>Type</th>
                                                        <th>Truck</th>
                                                        <th>Mjesec</th>
                                                        <th>Godina</th>
                                                        <th>Komentar</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @foreach ($stockpile as $s)
                                                    <tr>
                                                        <td>{{$s->code}}</td>
                                                        <td>{{$s->amount}}</td>
                                                        <td>{{$s->price}}</td>
                                                        <td>{{$s->total}}</td>
                                                        <td>{{$s->date}}</td>
                                                        <td>{{$s->exp_date}}</td>
                                                        <td>{{$s->type}}</td>
                                                        <td>{{$s->truck}}</td>
                                                        <td>{{$s->month}}</td>
                                                        <td>{{$s->year}}</td>
                                                        <td>{{$s->comment}}
                                                            <!-- <a href="edit.html" class="">edit</a>
                                                            <a href="#" class="">disable</a> -->
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    <tr>
                                                            <td><strong>Total:</strong></td>
                                                            <td><strong>{{$stockpile->sum('amount')}}</strong></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>
                                                                <!-- <a href="edit.html" class="">edit</a>
                                                                <a href="#" class="">disable</a> -->
                                                            </td>
                                                        </tr>

                                                </tbody>
                                            </table>
                                            <hr>

                                @php
                                        // dump($stockpile->groupBy('amount')->keys());
                                        $labels = $stockpile->groupBy('month')->keys();
                                        $values = $stockpile->groupBy('month')->map(function ($row)
                                        {
                                            return $row->sum('amount');
                                        })->values();


                                        // dump($labels);
                                        // dd($values);
                                        $chart->labels($labels)->options([
                                                'legend'=>[
                                                    'display' => true
                                                ],
                                                'title'=>[
                                                    'display' => true
                                                ],
                                                'animations'=>[
                                                    'easing' => 'easeInCubic',
                                                ],
                                            ]);
                                        ;
                                        $chart->dataset('Product stockpile by month', 'bar', $values)->options([
                                            'borderColor'   =>  '#3B5C5A',
                                            'color' => '#23E1BD',
                                            'backgroundColor' => '#26EDFF',

                                            // [
                                            //     '#26EDFF',
                                            //     '#23E1BD',
                                            // ]
                                        ]);


                                @endphp


                                <div class="row">
                                    TestChart:
                                        <div>{!! $chart->container() !!}</div>
                                        <script src=//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js charset=utf-8></script>
                                        {!! $chart->script() !!}
                                </div>

                                {{--  @foreach ($stockSales as $item)
                                    {{--  {{print_r($item->toArray())}}  --}}
                                    {{--  {{ $item->ataco_code }}         |  --}}
                                    {{--  {{ $item->kat_num }}            |  --}}
                                    {{--  {{ $item->opis }}               |  --}}
                                    {{--  {{ $item->prev_total_stock }}   |  --}}
                                    {{--  {{ $item->shipment }}           |  --}}
                                    {{--  {{ $item->sbrijeg }}            |  --}}
                                    {{--  {{ $item->bihac }}              |  --}}
                                    {{--  {{ $item->tuzla }}              |  --}}
                                    {{--  {{ $item->sarajevo }}           |  --}}
                                    {{--  {{ $item->laktasi }}            |  --}}
                                    {{--  {{ $item->total_stock }}        |  --}}
                                    {{--  {{ $item->type }}               |  --}}
                                    {{--  {{ $item->date }}               |  --}}
                                    {{--  {{ $item->code }}               |  --}}
                                    {{--  {{ $item->product_id }}         |  --}}
                                    {{--  {{ $item->acode_date_type }}    |  --}}
                                    {{--  {{ $item->month }}              |  --}}
                                    {{--  {{ $item->year }}               |  --}}
                                    {{--  <span class="btn-small btn-danger">{{ $item->error }}</span>              |  --}}
                                    {{--  {{ $item->average }}            |  --}}
                                    {{--  {{ $item->created_at }}         |
                                    {{ $item->updated_at }}         |  --}}
                                    <hr>
                                {{--  @endforeach   --}}
                            {{--  </pre>  --}}
                            </div>

                            <!-- end row -->

                            <div class="hidden-print">
                                <div class="pull-right">
                                    <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light">
                                        <i class="fa fa-print"></i>
                                    </a>
                                    <a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

@endif

@if($stockpile->count()==0)
    <h3>Nema zapisa!</h3>
@endif

@endsection