@extends('layouts.app')

@include('stockpile.menu')

@section('content')

<div class="row">

    <div class="card-box">

        <h4 class="header-title m-t-0 m-b-30">Insert stockpile</h4>

        <hr>

        <div class="row">
            <form action="/stockpile/createfromtext" method="POST">
                {{ csrf_field() }}
                <div class="col-md-12">
                        <div class="form-group">
                                {{--  <label class="col-md-2 control-label">Text area</label>  --}}
                                <div class="col-md-12">
                                    <textarea class="form-control" rows="5" cols="11" name="text" placeholder="Insert stockpile  koristeci csv formatirani text.  Iskopirajte stock ili sales pravo iz excela"></textarea>
                                </div>
                            </div>

            </div>

            <div class="col-md-12">

                    <h3>Info:</h3>
                    vrlo vazan je prvi red jer definise imena kolona u aplikaciji:
                    <pre>
code<i class="zmdi zmdi-long-arrow-tab text-danger"></i>title<i class="zmdi zmdi-long-arrow-tab text-danger"></i>amount<i class="zmdi zmdi-long-arrow-tab text-danger"></i>exp_date<i class="zmdi zmdi-long-arrow-tab text-danger"></i>price<i class="zmdi zmdi-long-arrow-tab text-danger"></i>total<i class="zmdi zmdi-long-arrow-tab text-danger"></i>truck<i class="zmdi zmdi-long-arrow-tab text-danger"></i>date<i class="zmdi zmdi-long-arrow-tab text-danger"></i>comment	type

<br>
code	title	amount	exp_date	price	total	truck	date	comment	type (comment, type - autoamtic just note @AHA)

</pre>

                    Primjer texta za unos stock and sales-a:


                <div class="col-md-12 m-t-30">
                    <button type="submit " class="btn btn-purple waves-effect waves-light ">Submit</button>
                </div>
            </div>

            </form>
        </div>


    </div>

</div>


@endsection