@section('sidebar')
    @parent
    <hr>

    @component('layouts.link',['link_text' => 'Stockpile create', 'link'=>'/stockpile/create'])
    @endcomponent
    @component('layouts.link',['link_text' => 'Stockpile all', 'link'=>'/stockpile'])
    @endcomponent

    @component('layouts.link',['link_text' => 'Stockpile search', 'link'=>'/stockpile/scan'])
    @endcomponent



@endsection