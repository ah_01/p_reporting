
@extends('layouts.app')

@include('stockpile.menu')

@section('content')

    <div class="row">

        <div class="card-box">

            <h4 class="header-title m-t-0 m-b-30">stock_sales</h4>
            <hr>
            <div class="row col-md-12">
                <form action="/stockpile" method="get">
                    {{ csrf_field() }}
                    <input type="hidden" name="q" value="q">

                    <div class="col-md-2 form-group">
                        <label class="control-label">Code</label>
                    <input type="text" class="form-control" name="code" value="{{request('code')}}">
                    </div>

                    <div class="col-md-1 form-group">
                        <label class="control-label">month</label>
                        <input type="text" class="form-control" name="month" value="{{request('month')}}">
                    </div>

                    <div class="col-md-1 form-group">
                        <label class="control-label">year</label>
                        <input type="text" class="form-control" name="year" value="{{request('year')}}">
                    </div>

                    <div class="col-md-1 form-group">
                        <label class="control-label">type</label>
                        <input type="text" class="form-control" name="type" value="{{request('type')}}">
                    </div>

                    <div class="col-md-1 form-group">
                        <label class="control-label">truck</label>
                        <input type="text" class="form-control" name="truck" value="{{request('truck')}}">
                    </div>


                    <div class="col-md-4">
                            <button type="submit " class="btn btn-purple waves-effect waves-light ">Submit</button>
                            <br>
                            @if (request('q'))
                                <a href="{{url()->full()}}&excel=1">Export Excel</a>
                            @endif
                    </div>

                </form>
                <div class="col-md-12">
                    @isset($stockSales)
                        {{$stockSales->count()}} stavki nadjeno
                    @endisset
                </div>
                <br>
                <br>
                <br>
                <hr>


        </div>

        @isset($stockpile)

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <!-- <div class="panel-heading">
                                                                                            <h4>Invoice</h4>
                                                                                        </div> -->
                        <div class="panel-body">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h2 class="">Stock Sales -
                                        <span class="text-danger">Stockpile</span>
                                        </h3>
                                </div>
                                <div class="pull-right">
                                    ...
                                </div>

                            </div>
                            @php

                            @endphp

                            <hr>

                                @foreach ($stockpile->chunk(3) as $chunk)
                                <div class="row">
                                            @foreach ($chunk as $product=>$key)
                                                <div class="col-xs-4">
                                                          <h5>{{$loop->parent->index}}. <a href="/stockpile/{{$product}}">{{$product}}</a> - unosa: {{$key->count()}} , total: {{$key->sum('amount')}}</h5>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            <hr>
                            <!-- end row -->

                            <div class="hidden-print">
                                <div class="pull-right">
                                    <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light">
                                        <i class="fa fa-print"></i>
                                    </a>
                                    <a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

@endisset

@isset($stockpile)
    <h3>Nema zapisa!</h3>
@endisset

@endsection