@extends('layouts.app')

@include('stock_sale.menu')

@section('content')
<ais-index
app-id="{{ config('scout.algolia.id') }}"
api-key="{{ config('scout.algolia.key') }}"
index-name="stocks_and_sales"
>

<div class="row">


        <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30">Basic example</h4>

                <div class="col-md-3">
                    <ais-search-box autofocus></ais-search-box>
                    Broj rezultata:<ais-results-per-page-selector :options="[20, 50, 100]"></ais-results-per-page-selector>
                </div>
                <div class="col-md-9">
                    <div class="row alert alert-info fade in m-b-0">
                        <h4>Refinement lists</h4>
                        <div class="col-md-6">
                            Godina: <ais-refinement-list attribute-name="year" ></ais-refinement-list>
                            Mjesec: <ais-refinement-list attribute-name="month"></ais-refinement-list>
                        </div>
                        <div class="col-md-6">
                            Tip:    <ais-refinement-list attribute-name="type"></ais-refinement-list>
                        </div>
                        <img src="{{asset('search-by-algolia.png')}}" alt="" width="90">
                    </div>

                    <ais-stats></ais-stats>
                    <hr>
                </div>







            <ais-results inline-template >


                <table class="table">
                     <thead>
                      <tr>
                        <th scope="col">Code</th>
                        <th scope="col">Date</th>
                        <th scope="col">Prev total stock</th>
                        <th scope="col">Shipment</th>
                        <th scope="col">sbrijeg</th>
                        <th scope="col">bihac</th>
                        <th scope="col">tuzla</th>
                        <th scope="col">sarajevo</th>
                        <th scope="col">laktasi</th>
                        <th scope="col">total_stock</th>
                        <th scope="col">average</th>
                        <th scope="col">Error</th>
                      </tr>
                    </thead>
                    <tbody>

                    <tr v-for="result in results" :key="result.objectID">

                        <td><a v-bind:href="/stockpile/+result.code" target="_blank">@{{ result.code }}</a> - @{{ result.type }}</td>
                        <td>@{{ result.date }}</td>
                        <td>@{{result.prev_total_stock}}</td>
                        <td>@{{result.shipment}}</td>
                        <td>@{{result.sbrijeg}}</td>
                        <td>@{{result.bihac}}</td>
                        <td>@{{result.tuzla}}</td>
                        <td>@{{result.sarajevo}}</td>
                        <td>@{{result.laktasi}}</td>
                        <td>@{{result.total_stock}}</td>
                        <td>@{{result.average}}</td>
                        <td>@{{ result.error }}</td>
                        {{-- <td>---</td> --}}
                      </tr>

                    </tbody>
                  </table>

              </ais-results>
            </div>

            <ais-pagination></ais-pagination>
            </div>
        </ais-index>


@endsection