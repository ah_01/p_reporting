@extends('layouts.app')

@section('sidebar')
    @parent
    <hr>
    @component('layouts.link',['link_text' => 'Proizvod-dodaj', 'link'=>'/product/create'])
    @endcomponent

@endsection

@section('content')

<div class="row">

    <div class="card-box">

        <h4 class="header-title m-t-0 m-b-30">Create Product</h4>

        <div class="row">
            <form action="/product/create" method="POST">
                {{ csrf_field() }}
                <div class="col-md-6">
                    <div class="form-group">
                        <label>name_eng</label>
                        <input type="text" class="form-control" name="name_eng" >
                    </div>
                    <div class="form-group">
                        <label>ataco_code</label>
                        <input type="text" class="form-control" name="ataco_code" >
                    </div>

                    <div class="form-group">
                        <label>code</label>
                        <input type="text" class="form-control" name="code" >
                    </div>

            </div>

            <div class="col-md-6">
                <p>Info:</p>
                <div class="col-md-12 m-t-30">
                    <button type="submit " class="btn btn-purple waves-effect waves-light ">Submit</button>
                </div>
            </div>

            </form>
        </div>

        <hr>

        <div class="row">
            <form action="/product/store" method="POST">
                {{ csrf_field() }}
                <div class="col-md-6">
                        <div class="form-group">
                                {{--  <label class="col-md-2 control-label">Text area</label>  --}}
                                <div class="col-md-10">
                                    <textarea class="form-control" rows="5" name="text">Add product using csv formatted text see info for example</textarea>
                                </div>
                            </div>

            </div>

            <div class="col-md-6">

                    <h3>Info:</h3>
                    <pre>
                      ataco_code<i class="zmdi zmdi-long-arrow-tab text-danger"></i>code<i class="zmdi zmdi-long-arrow-tab text-danger"></i>name_eng<br>
                      1110001<i class="zmdi zmdi-long-arrow-tab text-danger"></i>AL0002<i class="zmdi zmdi-long-arrow-tab text-danger"></i>Test Product<br>
                      1120001<i class="zmdi zmdi-long-arrow-tab text-danger"></i>HiPP0002<i class="zmdi zmdi-long-arrow-tab text-danger"></i>HiPP test
                    </pre>

                <div class="col-md-12 m-t-30">
                    <button type="submit " class="btn btn-purple waves-effect waves-light ">Submit</button>
                </div>
            </div>

            </form>
        </div>


    </div>

</div>


@endsection