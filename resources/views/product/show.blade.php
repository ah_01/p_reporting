@extends('layouts.app')

@section('sidebar')
    @parent
    <hr>
    @component('layouts.link',['link_text' => 'Proizvod-dodaj', 'link'=>'/product/create'])
    @endcomponent

@endsection

@section('content')

<div class="row">

    <div class="card-box">

        <h4 class="header-title m-t-0 m-b-30">Dodaj proizvod</h4>

        <div class="row">
            <form action="/product/{{$product->id}}" method="POST">
                {{ csrf_field() }}
                <div class="col-md-6">

                    <div class="form-group">
                        <label>name_eng</label>
                        <input type="text" class="form-control" name="name_eng" value="{{$product->name_eng}}" >
                    </div>

                    <div class="form-group">
                        <label>name_ba</label>
                        <input type="text" class="form-control" name="name_ba" value="{{$product->name_ba}}" >
                    </div>

                    <div class="form-group">
                        <label>country</label>
                        <input type="text" class="form-control" name="country" value="{{$product->country}}" >
                    </div>

                    <div class="form-group">
                        <label>ataco_code</label>
                        <input type="text" class="form-control" name="ataco_code" value="{{$product->ataco_code}}" >
                    </div>

                    <div class="form-group">
                        <label>code</label>
                        <input type="text" class="form-control" name="code" value="{{$product->code}}" >
                    </div>

                    <div class="form-group">
                        <label>brand</label>
                        <input type="text" class="form-control" name="brand" value="{{$product->brand}}" >
                    </div>

                    <div class="form-group">
                        <label>category</label>
                        <input type="text" class="form-control" name="category" value="{{$product->category}}" >
                    </div>


                    <div class="form-group">
                        <label>em_no</label>
                        <input type="text" class="form-control" name="em_no" value="{{$product->em_no}}" >
                    </div>

                    <div class="form-group">
                        <label>type</label>
                        <input type="text" class="form-control" name="type" value="{{$product->type}}" >
                    </div>

                    <div class="form-group">
                            <label>status</label>
                            <input type="text" class="form-control" name="status" value="{{$product->status}}" >
                    </div>

                    <div class="form-group">
                            <label>price</label>
                            <input type="text" class="form-control" name="price" value="{{$product->price}}" >
                    </div>

                    <div class="form-group">
                            <label>comment</label>
                            <input type="text" class="form-control" name="comment" value="{{$product->comment}}" >
                    </div>
                    <p>Stranica:{{$product->stranica}}</p>


            </div>

            <div class="col-md-6">
                <p>Info:</p>
                <div class="col-md-12 m-t-30">
                    <button type="submit " class="btn btn-purple waves-effect waves-light ">Submit</button>
                </div>
            </div>

            </form>
        </div>


    </div>

</div>


@endsection