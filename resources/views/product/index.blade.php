@extends('layouts.appa')

@section('sidebar')
    @parent
    <hr>
    @component('layouts.link',['link_text' => 'Proizvod-dodaj', 'link'=>'/product/create'])
    @endcomponent

@endsection

@section('content')


<div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>
                    {{--  <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>  --}}
                </div>

                <h1 class="header-title m-t-0 m-b-30">Products:</h1>
{{--
                <div class="row">
                    TestChart:
                        <div>{!! $chart->container() !!}</div>
                        <script src=//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js charset=utf-8></script>
                        {!! $chart->script() !!}
                </div> --}}
                <hr>
                <table id="datatable-keytable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Ataco_code</th>
                            <th>Name_eng</th>
                            <th>Brand</th>
                            <th>Kategorija</th>
                            <th>Status</th>

                            <th>Web</th>
                            <th>Akcije</th>
                        </tr>
                    </thead>

                    <tbody>

                        @foreach ($products as $product)
                        <tr>
                            <td><a href="stockpile?&q=q&code={{$product->code}}">{{$product->code}}</a></td>
                            <td>{{$product->ataco_code}}</td>
                            <td>{{$product->name_eng}}</td>
                            <td>{{$product->brand}}</td>
                            <td>{{$product->category}}</td>
                            <td>{{$product->status}}</td>

                            <td>W:{{$product->stranica}}</td>
                            <td>
                                @php
                                    $color = ($product->stranica == 1) ? "btn-success" : "btn-danger" ;
                                @endphp
                                {{--  <a href="/product/{{$product->id}}" class="">edit</a>  --}}
                                <a href="/product/{{$product->id}}/stranica" class="btn {{$color}} waves-effect waves-light btn-xs m-b-5">w</a>
                                <a href="/product/{{$product->id}}" class="btn btn-info waves-effect waves-light btn-xs m-b-5">Edit</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>



@endsection

@section('css')
    <!-- DataTables -->
    {{--  <link href="{{asset('text')}}" rel="stylesheet" type="text/css" />  --}}

    <link href="{{asset('assets/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/datatables/fixedHeader.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/datatables/scroller.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('js')
    {{--  <script src="txt"></script>  --}}
    <!-- Datatables-->
    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>
    <script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="assets/plugins/datatables/buttons.bootstrap.min.js"></script>
    <script src="assets/plugins/datatables/jszip.min.js"></script>
    <script src="assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="assets/plugins/datatables/buttons.print.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.keyTable.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="assets/plugins/datatables/responsive.bootstrap.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.scroller.min.js"></script>


    <!-- Datatable init js -->
    <script src="assets/pages/datatables.init.js"></script>

@endsection

@section('jscript')
    $(document).ready(function() {
        $('#datatable-keytable').DataTable( { keys: true } );
    } );
@endsection