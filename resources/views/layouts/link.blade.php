<li>
    <a href="{{$link or '/'}}" class="waves-effect">
        <i class="zmdi zmdi-view-dashboard"></i>
        <span> {{$link_text or 'Link Text'}} </span>
    </a>
</li>