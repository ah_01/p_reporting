<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes" g>

    <!-- App Favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- App title -->
    <title>Reporting - @yield('title')</title>

    <!-- App CSS -->

    <link href="{{ asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')  }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/multiselect/css/multi-select.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/select2/dist/css/select2.css')  }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/select2/dist/css/select2-bootstrap.css')  }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')  }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/switchery/switchery.min.css')  }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/timepicker/bootstrap-timepicker.min.css')  }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')  }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')  }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')  }}" rel="stylesheet">

    @yield('css')

    <!-- App CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />



    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>

</head>


<body class="fixed-left">
    <!-- Begin page -->
    <div id="wrapper">

        @include('layouts.topbar')

        @include('layouts.leftsidebar')






        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container" id="app">

                    @yield('content')


                </div>
                <!-- container -->
                <!-- container -->

            </div>
            <!-- content -->

            <footer class="footer">
                2016 © Adminto.
            </footer>

        </div>
        <!-- End content-page -->


        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->


    <!-- jQuery  -->

    <!-- jQuery  -->
    <!-- production version, optimized for size and speed -->
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>

    <script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>


</body>

</html>