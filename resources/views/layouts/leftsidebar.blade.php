        <!-- ========== Left Sidebar Start ========== -->
        <div class="left side-menu">
            <div class="sidebar-inner slimscrollleft">

                @auth

                <!--- Sidemenu -->
                <div id="sidebar-menu">
                        <ul>
                            <li class="text-muted menu-title">Navigation</li>
                            <li>
                                <a href="/product" class="waves-effect">
                                    <i class="zmdi zmdi-view-dashboard"></i>
                                    <span> Product </span>
                                </a>
                            </li>
                            <li>
                                <a href="/" class="waves-effect">
                                    <i class="zmdi zmdi-view-dashboard"></i>
                                    <span> StockSales </span>
                                </a>
                            </li>
                            </li>
                            <li>
                                <a href="/stockpile" class="waves-effect">
                                    <i class="zmdi zmdi-view-dashboard"></i>
                                    <span> Stockpile </span>
                                </a>
                            </li>
                            <hr>
                            <li>
                                <a href="/reports" class="waves-effect">
                                    <i class="zmdi zmdi-folder"></i>
                                    <span> Reports </span>
                                </a>
                            </li>

                            @section('sidebar')
                            @show

                            {{--  <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect active">
                                    <i class="zmdi zmdi-collection-item"></i>
                                    <span> Pages </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="list-unstyled">
                                    <!-- <li class="active">
                                        <a href="edit_1.html">Edit/Show truck</a>
                                    </li> -->




                                </ul>
                            </li>  --}}




                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>
                @endauth

            </div>

        </div>
        <!-- Left Sidebar End -->