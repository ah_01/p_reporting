
@extends('layouts.app')


@section('sidebar')
    @parent

    @component('layouts.link',['link_text' => 'Kamioni', 'link'=>'#'])
    @endcomponent


    @component('layouts.link',['link_text' => 'Kamioni-dodaj', 'link'=>'/truck/create'])
    @endcomponent

@endsection

@section('content')

<div class="card-box">

<div class="dropdown pull-right">
    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
        <i class="zmdi zmdi-more-vert"></i>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="#">Action</a>
        </li>
        <li>
            <a href="#">Another action</a>
        </li>
        <li>
            <a href="#">Something else here</a>
        </li>
        <li class="divider"></li>
        <li>
            <a href="#">Separated link</a>
        </li>
    </ul>
</div>

<h4 class="header-title m-t-0 m-b-30">Create truck</h4>

<div class="row">
    <div class="col-lg-6">
        <form class="form-horizontal" role="form" action="/truck/create" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label class="col-md-2 control-label">Truck number</label>
                <div class="col-md-10">
                    <input type="number" class="form-control" value="{{ $truck->number or '' }}" name="number">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">year</label>
                <div class="col-sm-10">
                    <select class="form-control" name="year" value="{{ $truck->year or '' }}" >
                        <option>2018</option>
                        <option>2019</option>
                        <option>2020</option>
                        <option>2021</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Invoice number</label>
                <div class="col-md-10">
                    <input type="invoice_number" class="form-control" placeholder="11110001" name="invoice_number"
                    value="{{ $truck->invoice_number or '' }}" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">Date</label>
                <div class="col-md-10">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="mm-dd-yyyy" id="datepicker" name="delivery_date" value="{{ $truck->delivery_date or '' }}" >
                        <span class="input-group-addon bg-primary b-0 text-white">
                            <i class="ti-calendar"></i>
                        </span>
                    </div>
                    <!-- input-group -->
                </div>
            </div>
            <button type="submit" class="btn btn-purple waves-effect waves-light">Submit</button>
        </form>
    </div>
    <!-- end col -->

    <div class="col-lg-6">

        <h3>Objasnjenje</h3>
        <p>
            //TODO: Uraditi text objasnjenja
        </p>

    </div>
    <!-- end col -->

</div>
<!-- end row -->
</div>
@endsection