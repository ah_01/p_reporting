
@extends('layouts.app')


@section('sidebar')
    @parent

    @component('layouts.link',['link_text' => 'Kamioni', 'link'=>'#'])
    @endcomponent


    @component('layouts.link',['link_text' => 'Kamioni-dodaj', 'link'=>'/truck/create'])
    @endcomponent

@endsection


@section('content')
<div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <!-- <div class="panel-heading">
                                                            <h4>Invoice</h4>
                                                        </div> -->
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="pull-left">
                                            <h2 class="">Trucks</h3>
                                        </div>
                                        <div class="pull-right">
                                            ...
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">

                                    </div>
                                    <!-- end row -->

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card-box">
                                                <table class="table table-striped m-t-30">
                                                    <thead>
                                                        <tr>
                                                            <th>Number</th>
                                                            <th>Year</th>
                                                            <th>Delivery date</th>
                                                            <th>Invoice number</th>
                                                            <th>Number_Year</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($trucks as $truck)

                                                            <tr>
                                                                <td>{{$truck->number}}</td>
                                                                <td>{{$truck->year}}</td>
                                                                <td>{{$truck->delivery_date}}</td>
                                                                <td>{{$truck->invoice_number}}</td>
                                                                <td>
                                                                    <a href="/truck/{{$truck->id}}" class="btn btn-primary waves-effect w-md waves-light m-b-5">{{ $truck->number_year }}</a>
                                                                </td>
                                                            </tr>

                                                        @endforeach



                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <hr> -->
                                    <!-- <div class="hidden-print">
                                        <div class="pull-right">
                                            <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light">
                                                <i class="fa fa-print"></i>
                                            </a>
                                            <a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>
                                        </div>
                                    </div> -->
                                </div>
                            </div>

                        </div>

                    </div>
@endsection