
@extends('layouts.app')


@section('sidebar')
    @parent

    @component('layouts.link',['link_text' => 'Invoices', 'link'=>'/invoice'])
    @endcomponent


    @component('layouts.link',['link_text' => 'Invoices-dodaj', 'link'=>'/invoice/create'])
    @endcomponent

@endsection


@section('content')
<div class="row">

    <div class="card-box">

        <h4 class="header-title m-t-5 m-b-2">Create Invoice</h4>
        <div class="row">

            <div class="col-md-9">
                <form action="/invoice/create" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label>Invoice_number</label>
                        <input type="text" placeholder="1111111" class="form-control" name="invoice_number">
                        <span class="font-13 text-muted">e.g </span>
                    </div>
                    <div class="form-group">
                        <label>Delivery_Date</label>
                        <input placeholder="00.00.0000" class="form-control" id="datepicker" name="delivery_date">
                        <span class="font-13 text-muted">e.g. 2.2.2000</span>
                    </div>
                    <button type="submit" class="btn btn-purple waves-effect waves-light">Submit</button>
                </form>
            </div>

            <div class="col-md-3">
                <h3>Info</h3>
                <p>
                    //TODO: Make relation Truck->Invoice! Popuniti kamion i invoice info prvo ! Onda napraviti invoice ovdje kako bi mogli dodati
                    aktuelne podatke
                </p>
            </div>

        </div>
        <!-- end col -->
    </div>
    </div>
@endsection