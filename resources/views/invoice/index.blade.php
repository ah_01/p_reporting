
@extends('layouts.app')


@section('sidebar')
    @parent

    @component('layouts.link',['link_text' => 'Invoices', 'link'=>'/invoice'])
    @endcomponent


    @component('layouts.link',['link_text' => 'Invoices-dodaj', 'link'=>'/invoice/create'])
    @endcomponent

@endsection


@section('content')
<div class="row">
        <div class="panel panel-default">
        <!-- <div class="panel-heading">
                                            <h4>Invoice</h4>
                                        </div> -->
        <div class="panel-body">
            <div class="clearfix">
                <div class="pull-left">
                    <h2 class="">Invoices</h2>
                </div>
                <div class="pull-right">
                    ...
                </div>
            </div>
            <hr>
            <div class="row">

            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <table class="table table-striped m-t-30">
                            <thead>
                                <tr>
                                    <th>invoice_number</th>
                                    <th>delivery_date</th>
                                <th>Akcije</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($invoices as $invoice)
                                    <tr>
                                        <td>{{$invoice->invoice_number}}</td>
                                        <td>{{$invoice->delivery_date}}</td>
                                        <td><a href="/invoice/{{$invoice->id}}" class="btn btn-primary waves-effect w-md waves-light m-b-5">Izmjeni</a></td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- <hr> -->
            <!-- <div class="hidden-print">
                        <div class="pull-right">
                            <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light">
                                <i class="fa fa-print"></i>
                            </a>
                            <a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>
                        </div>
                    </div> -->
        </div>
        </div>
    </div>
@endsection