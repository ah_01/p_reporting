<?php
/*
Auth::loginUsingId(1);
Auth::logOut();
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::group(['middleware' => ['auth']], function () {
    Route::get('/stocksales_json', function () {
        return view('stock_sale.json1', ['url'=>public_path("stocksales.json")]);
    });
    Route::get('/', 'StockSalesController@index');

    Route::get('/invoice', 'InvoiceController@index');
    Route::get('/invoice/create', 'InvoiceController@show');
    Route::get('/invoice/{id}', 'InvoiceController@show');
    Route::post('/invoice/create', 'InvoiceController@create');

    Route::get('/', 'StockSalesController@index');
    Route::get('/stocksales', 'StockSalesController@index');
    Route::post('/stocksales/create', 'StockSalesController@create');
    Route::post('/stocksales/createfromtext', 'StockSalesController@store');
    Route::get('/stocksales/create', 'StockSalesController@show');
    Route::get('/stocksales/json', 'StockSalesController@index');
    Route::get('/stocksales/check', 'StockSalesController@index');
    Route::get('/stocksales/all', 'StockSalesController@index');
    Route::get('/stocksales/search', 'SearchController@show');
    Route::get('/stocksales/scan', 'SearchController@index');


    Route::get('/stock', 'StockController@index');
    Route::get('/stock/json', 'StockController@index');

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/truck', 'TruckController@index');
    Route::post('/truck/create', 'TruckController@create');
    Route::get('/truck/create', 'TruckController@show');
    Route::get('/truck/{id}', 'TruckController@show')->name('truck.show');
    Route::post('/truck/{id}', 'TruckController@edit');

    Route::get('/product', 'ProductController@index');
    Route::get('/product/create', 'ProductController@create');
    Route::post('/product/create', 'ProductController@create');
    Route::post('/product/store', 'ProductController@store');
    Route::get('/product/{id}', 'ProductController@show');
    Route::get('/product/{id}/stranica', 'ProductController@stranica');
    Route::post('/product/{id}', 'ProductController@update');

    Route::post('/stockpile/create', 'StockpileController@create');
    Route::get('/stockpile/create', 'StockpileController@show');
    Route::get('/stockpile/{stockpile}', 'StockpileController@product');
    Route::post('/stockpile/createfromtext', 'StockpileController@store');
    Route::get('/stockpile', 'StockpileController@index');

    Route::get('/reports', 'ReportsController@index');
    Route::get('/reports/running', 'ReportsController@running');
    Route::get('/reports/pie', 'ReportsController@pie');
});
Route::get('/test_data', 'ChartsController@response');
// Route::get('/test_data/{name}{type}{value}', 'ChartsController@response');
