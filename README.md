###Reporting Features

####Truck
- *Create Truck*
- *Edit/Update truck truck - Done*
- Delete - for later

####Invoice
 - *Create*
 - Edit
     - update invoice
     - attach products from text
     - when attaching products insert them also to Stock as "Stock" type
     - rewrite Invoice add products from text
 - Delete

####Product
 - Create
     - ***Product info(CODE, ATACO_CODE, NAME, BRAND, STATUS, WEB, ACTIONS) - DONE***
     - ***Data table for easier filtering sorting - DONE***
     - *mass create products from csv ! Based on Ataco_code, Code, Name - Done*
     - ***FeatureTest: create products from text (done)***
     - ***FeatureTest: create product form - DONE***

  - Edit/Update
      - ***Feature: edit/update product form -DONE***

  - Delete - later


####Stock_Sale
 - Create
     Products must be created to create stock and sale!!! DIsable this workflow for now ?!!!
     ***Disabled***
     For easier shipping of initial part stock and sales could be created without product dependency
     later addition to current stock should be dependent on product.
     KEEP: all code used up to now  because it will be used again.

     Possible new workflow:
     - Add stock_sales no dependency
     - calculate sales average during import
     - check common errors on stock_sale and mark that record as error record
       (
           This will be used only as identifier at start, while mnore robust error reporting is not added
           Errors:
            - average sale is bigger or very close to current stock - oos
            - total_stock (sale) and total_stock are not equal to prev_total_stock
            - this marking will give focus for products that need to be checked
            - only reports with errors will be displayed initialy
            - add errors reporting as separate module (table, events ...)
       )
     - Following previous changes add:
        - filtering of records based on month, year, product
        - create reports based on single product

**----**
     - after adding stock sales manually trigger add to current_stock, based on following options(only active product will be added to current stock, only product that has some numbers in sales?), this would keep current_stock a bit cleaner...

 - *When creating stock_sale insert products of type sale to Stock as negative value*
 - View - TODO:
 - Json view, save json file - Stays for now
 - JS Pivot Table view - omit from reporting of all products - to slow, Apply it on smaller scale i.e. on specific product or month?

####Stock
 - View
 - Json View
 - JS Pivot View

####Other:
 - Add user command line