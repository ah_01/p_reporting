<?php
namespace App\Hipp\Search;

use Illuminate\Support\ServiceProvider;

class SearchServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('search', 'App\Hipp\Search\Search');
    }
}
