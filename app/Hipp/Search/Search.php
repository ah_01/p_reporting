<?php
namespace App\Hipp\Search;

use App\HiPP\Exports\Export;

use App\Stock_Sale;
use App\Stockpile;

class Search
{
    public function posts($search = null)
    {
        dd("POSTS");
    }

    public function reporting()
    {
        # code...
    }
    public function stocksale($search)
    {
        // dd($search);
        $q = Stock_Sale::query();
        $q->select('ataco_code', 'code', 'opis', 'prev_total_stock', 'total_stock', 'average', 'type', 'error', 'date', 'month', 'year');

        $q = (request('code')) ? $q->SearchCode(request('code')) : $q ;
        $q = (request('date')) ? $q->SearchDate(request('date')) : $q ;
        $q = (request('month')) ? $q->SearchMonth(request('month')) : $q ;
        $q = (request('year')) ? $q->SearchYear(request('year')) : $q ;
        $q = (request('type')) ? $q->SearchType(request('type')) : $q ;
        $q = (request('error')) ? $q->SearchError(request('error')) : $q ;

        return $q;
    }
    public function stockpile($search)
    {
        // dd($search);
        $q = Stockpile::query();
        $q->select('code', 'amount', 'price', 'total', 'date', 'exp_date', 'type', 'truck', 'month', 'year');

        $q = (request('code')) ? $q->SearchCode(request('code')) : $q ;
        $q = (request('month')) ? $q->SearchMonth(request('month')) : $q ;
        $q = (request('year')) ? $q->SearchYear(request('year')) : $q ;
        $q = (request('type')) ? $q->SearchType(request('type')) : $q ;
        $q = (request('truck')) ? $q->SearchTruck(request('truck')) : $q ;

        return $q;
    }
}
