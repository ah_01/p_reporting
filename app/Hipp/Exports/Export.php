<?php
namespace App\HiPP\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use App\Stock_Sale;

class Export implements FromQuery, WithHeadings
{
    use Exportable;
    public function __construct()
    {
        $this->Stock_Sale = new Stock_Sale;
    }

    public function headings(): array
    {
        return [
            'ataco_code', 'code', 'opis', 'prev_total_stock', 'total_stock', 'type', 'error','date', 'month', 'year'
        ];
    }


    public function forQ($q)
    {
        $this->q = $q;

        return $this;
    }

    public function query()
    {
        return $this->q;
    }
}
