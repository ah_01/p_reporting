<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Truck extends Model
{
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['delivery_date'];
    //

    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->number_year = $model->number . '_' . $model->year;
        });
    }

    public function setNumberYearAttribute()
    {
        return $this->attributes['number_year'] = $this->number . '_' . $this->year;
    }
}
