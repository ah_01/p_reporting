<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Stockpile extends Model
{
    protected $dateFormat = 'd.m.Y';
    protected $dates =['date', 'exp_date'];
    protected $guarded =[];
    public $timestamps =false;


    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            // dd($model->toArray());
            // dd(Carbon::parse($model->date));
            $date = Carbon::parse($model->date);
            $model->year = $date->year;
            $model->month = $date->month;

            $code = explode('-', $model->code, 2);

            $p = Product::where('code', '=', $code[0])->get();
            // dd(Product::all()->toArray());
            // dd($p);
            $model->type = ($p->count()>0) ? "product" : "OTHER" ;
            if ($p->count()>0) {
                if ($p->first()->brand == 'Reklamni') {
                    $model->type = 'REKLAMNI';
                } elseif ($p->first()->brand == 'Sample') {
                    $model->type = 'SAMPLE';
                } else {
                    $model->type = 'product';
                }
            }
        });
    }

    public static function getFromText($text)
    {
        //TODO:  Extract csv creation logic to helper
        // Extract csv creation logic to helper
        $csv = collect(explode("\n", $text));
        $csv = $csv->map(function ($item) {
            return str_getcsv($item, "\t");
        });
        $fields = $csv->shift();
        $fields = array_map('strtolower', $fields);
        $fields = array_map(function ($value) {
            return str_replace([',', '.'], '', $value);
        }, $fields);

        // dd($fields);

        $csv = $csv->map(function ($item) use ($fields) {
            $i=0;
            $p=[];
            foreach ($item as $key => $value) {
                if ($fields[$i] != 'price') {
                    $p[$fields[$i]]= str_replace(',', '', $value);
                } else {
                    $p[$fields[$i]] = number_format((double)str_replace(',', '.', $value) * 1000);
                }
                $i++;
            }
            // dd($p);
            return $p;
        });
        // dd($csv);
        //TODOEND:  --------------------------------------------------------------------
        // check if product does not exists - if not made new collection fo structur NONE-CODE
        $check = $csv->groupBy('ataco_code')->keys()->filter(function ($value, $key) {
            // dump($value);
            return starts_with($value, 'NONE-');
        });
        // if check count is grateer than 0 throw exception with list of nonexistent codes
        if ($check->count()>0) {
            throw new \Exception("Product does not exists!" . $check, 1);
        }

        return $csv;
    }

    public function getPriceAttribute($price)
    {
        // dd($price);
        return number_format((float)$price / 1000, 3);
    }


    public static function saveEntries($arr)
    {
        return $arr->each(function ($entry) {
            self::create($entry);
        });
    }

    public function getDateAttribute($value)
    {
        // dd(Carbon::parse($value));
        return Carbon::parse($value)->toDateString();
    }

    public function getExpDateAttribute($value)
    {
        // dd(Carbon::parse($value));
        return Carbon::parse($value)->toDateString();
    }

    public function getDateFormat()
    {
        return 'd.m.Y';
    }

    public static function getTotal($code)
    {
        return self::where('code', '=', $code)->get()->sum('amount');
    }

    public static function getNumVal($string)
    {
        $string = str_replace([",", "."], ".", $string);
        $a =explode(".", $string);
        $a[0] *= 1000;
        if (!array_key_exists(1, $a)) {
            $a[1] = 0;
        }
        return $a[0] + $a[1];
        // dd($a);
    }


    public function scopeSearchCode($query, $search)
    {
        return $query->where(
            function ($query) use ($search) {
                $query->where('code', 'LIKE', "%$search%");
            }
        );
    }

    public function scopeSearchYear($query, $search)
    {
        return $query->where(
            function ($query) use ($search) {
                $query->where('year', 'LIKE', "%$search%");
            }
        );
    }

    public function scopeSearchMonth($query, $search)
    {
        $months = explode(',', $search);
        if (count($months) == 1) {
            $search = $months[0];
            return $query->where(
                function ($query) use ($search) {
                    $query->where('month', 'LIKE', "%$search%");
                }
            );
        # code...
        } else {
            // dd("multiple search");
            return $query->where(
                function ($query) use ($months) {
                    $query->whereBetween('month', $months);
                }
            );
        }
    }

    public function scopeSearchType($query, $search)
    {
        return $query->where(
            function ($query) use ($search) {
                $query->where('type', 'LIKE', "%$search%");
            }
        );
    }

    public function scopeSearchTruck($query, $search)
    {
        return $query->where(
            function ($query) use ($search) {
                $query->where('truck', 'LIKE', "%$search%");
            }
        );
    }

    public static function reCheck($code=null, $type=null)
    {
        if ($code == null && $type == null) {
            $stockPile = self::all();
        } elseif ($type != null) {
            $stockPile = self::where('type', '=', $type)->get();
        } else {
            $stockPile = self::where('code', '=', $code)->get();
            // dd($stockPile->toArray());
        }
        $stockPile->each(function ($item) {
            $item->type =null;
            $item->save();
            echo('.');
        });
    }


    public function scopeStock($query, $currentYear, $type, $month = null)
    {
        return $query->where(
            function ($query) use ($currentYear,$type,  $month) {
                $query->where('type', '=', $type);
                $query->where('year', '=', $currentYear);
                if ($month != null) {
                    $query->where('month', '=', $month);
                }
            }
        );
    }

    public static function runningTotal($initial_stock, $year, $month)
    {
        $month_product_total = 0;
        for ($i=1; $i < $month+1; $i++) {
            $month_product_total += self::Stock($year, 'product', $i)->get()->sum('amount')
                - Stock_Sale::Type($year, 'sale', $i)->get()->sum('total_stock');
            ;
        }

        return $initial_stock + $month_product_total;
    }
}
