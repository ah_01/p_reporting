<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;
use App\Events\StockSalesSaved;
use League\Flysystem\Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Carbon;
use Laravel\Scout\Searchable;

class Stock_Sale extends Model
{
    use Searchable;

    protected $table="stocks_and_sales";
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates =['date'];
    protected $guarded =[
        'redni',
    ];

    // protected $dispatchesEvents =[
    //     'creating'  =>  StockSalesSaved::class,
    // ];

    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            // $p = Product::where('ataco_code', $model->ataco_code)->firstOrFail();
            // dump("SAAAAVIIIING");
            // dump($model->ataco_code);
            $product = $model->getProduct($model->ataco_code);
            if ($product != null) {
                $productCode = $product->code;
                $productCategory = $product->category;
                $productId = $product->id;
                $productBrand = $product->brand;
            } else {
                $productCode = "NO CODE";
                $productCategory = "NO CATEGORY";
                $productBrand = 'NONE';
                $productId = null;
            }
            // dump($productCode);
            // dump($productCategory);

            $model->code = $productCode;
            $model->product_category = $productCategory;
            $model->product_brand = $productBrand;
            $model->product_id = $productId;

            $model->acode_date_type = $model->ataco_code . '_' .$model->date->toDateString() .'_'.$model->type;
            $model->year = $model->date->year;
            $model->month = $model->date->month;

            $total_stock_sale = $model->sbrijeg + $model->bihac + $model->tuzla + $model->sarajevo + $model->laktasi;

            $types_arr = [
                'Stock',
                'Sale'
            ];

            if ($total_stock_sale == 0 and in_array($model->type, $types_arr)) {
                return false;
            }
            $model->average = ($model->type == "Sale") ? round($total_stock_sale/5) : 0 ;

            if ($model->type == "Stock") {

                // get part of array with ataco stores
                $poslovnice = array_slice($model->getAttributes(), 5, 5);
                // dd(array_slice($model->getAttributes(), 5, 5));
                foreach ($poslovnice as $name => $value) {
                    // dump($name . ' - '. $value / $total_stock_sale);
                    $error = 0;
                    if (($value / $total_stock_sale >= 0.4)) {
                        $error = $name . '-over';
                        break;
                    }
                }

                $model->error = $error;
                // dd($error);
            }

            if ($model->type == "Sale") {
                if ($model->average * 3 > $model->getTotalSaleStock($model->ataco_code, $model->date->toDateString())) {
                    $model->error = "oos";
                }
            }

            foreach ($model->attributes as $key => $value) {
                $model->{$key} = empty($value) ? 0 : $value;
            }
        });
    }


    public function product()
    {
        // TODO: do manual adding of sales to current stock table
        // return null;
        return $this->belongsTo('App\Product');
    }
    /*
    * Functoon to retrieve code of product when no relationship between stock_sales and products exist
    * TODO: do internal checks for case where product is not avilable
    *
    * @param <div string=""></div> $ataco_code
    * @return [string] $code
    */
    public function getProduct($ataco_code)
    {
        try {
            $p =  Product::where('ataco_code', $ataco_code)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return null;
        }
        return $p;

        // return Product::where('ataco_code', $ataco_code)->firstOrFail()->code;
    }

    public static function getStockSalesFromText($text)
    {
        //TODO:  Extract csv creation logic to helper
        // Extract csv creation logic to helper
        $csv = collect(explode("\n", $text));
        $csv = $csv->map(function ($item) {
            return str_getcsv($item, "\t");
        });
        $fields = $csv->shift();
        $fields = array_map('strtolower', $fields);
        $fields = array_map(function ($value) {
            return str_replace([',', '.'], '', $value);
        }, $fields);

        // dd($fields);

        $csv = $csv->map(function ($item) use ($fields) {
            $i=0;
            $p=[];
            foreach ($item as $key => $value) {
                $p[$fields[$i]]= str_replace(',', '', $value);
                $i++;
            }
            // dd($p);
            return $p;
        });
        // dd($csv);
        //TODOEND:  --------------------------------------------------------------------
        // check if product does not exists - if not made new collection fo structur NONE-CODE
        $check = $csv->groupBy('ataco_code')->keys()->filter(function ($value, $key) {
            // dump($value);
            return starts_with($value, 'NONE-');
        });
        // if check count is grateer than 0 throw exception with list of nonexistent codes
        if ($check->count()>0) {
            throw new \Exception("Product does not exists!" . $check, 1);
        }

        return $csv;
    }

    // public static function getStockSalesFromText($text)
    // {
    //     //TODO:  Extract csv creation logic to helper
    //     // Extract csv creation logic to helper
    //     $csv = collect(explode("\n", $text));
    //     $csv = $csv->map(function ($item) {
    //         return str_getcsv($item, "\t");
    //     });
    //     $fields = $csv->shift();
    //     $fields = array_map('strtolower', $fields);
    //     $fields = array_map(function ($value) {
    //         return str_replace(['.',',00'], '', $value);
    //     }, $fields);

    //     // dd($fields);

    //     $csv = $csv->map(function ($item) use ($fields) {
    //         $i=0;
    //         $p=[];
    //         foreach ($item as $key => $value) {
    //             if ($i == 2) {
    //                 // dump($value);
    //                 $value = Product::where('ataco_code', $value)->exists() ? $value : 'NONE-' . $value ;
    //             }
    //             if ($i >= 4 and $i <= 11) {
    //                 $value = str_replace(['.',',00'], '', $value);
    //                 $value = str_replace([','], '.', $value);
    //                 // $value = $value * 100;
    //             }
    //             $p[$fields[$i]]=$value;
    //             $i++;
    //         }
    //         array_forget($p, ['redni', 'kataloski']);
    //         // dd($p);
    //         return $p;
    //     });
    //     // dd($csv);
    //     //TODOEND:  --------------------------------------------------------------------
    //     // check if product does not exists - if not made new collection fo structur NONE-CODE
    //     $check = $csv->groupBy('ataco_code')->keys()->filter(function ($value, $key) {
    //         // dump($value);
    //         return starts_with($value, 'NONE-');
    //     });
    //     // if check count is grateer than 0 throw exception with list of nonexistent codes
    //     if ($check->count()>0) {
    //         throw new \Exception("Product does not exists!" . $check, 1);
    //     }

    //     return $csv;
    // }


    public static function saveEntries($arr)
    {
        return $arr->each(function ($entry) {
            self::create($entry);
        });
    }

    public function getTotalSaleStock($atacoCode, $date)
    {
        // dump($atacoCode);
        // $atacoCode = 10001050;
        if (Stock_Sale::count()>0) {
            # code...
            $r = $this::where('ataco_code', '=', $atacoCode)
            ->where('type', '=', 'Stock')
            ->whereDate('date', '=', $date)
            ->get()->first();

            // dd($r);
            if ($r === null) {
                return 0;
            }
            return $r->total_stock;
        }
        return ;
    }

    public static function checkForErrors()
    {
        $all = self::Where('error', 'like', '0')->where('type', '!=', 'Stock_Start')->get()->groupBy(function ($item) {
            return $item->date->toDateString();
        })->map(function ($item, $key) {
            return $item->groupBy('ataco_code');
        }, $preserveKeys = true);
        ;
        if ($all->count()==0) {
            dump('no_Errors');
            return;
        }
        // dd($all);
        $all->each(function ($date, $key) {
            $date->each(function ($items, $key) {
                if ($items->count()!=2) {
                    // dd($items);
                    throw new Exception("There is odd number in here!" . $items->toJson(), 1);
                }
                $items->first()->checkError($items);
            });
        });
    }
    public function checkError($items)
    {
        //there should be only 2 elements in $items
        // one stock and one sale
        // calc is: PREV_TOTAL_STOCK(Stock) != TOTAL_STOCK(Stock) + TOTAL_STOCK(Sale) -> ERROR = TOTALS
        $sale = $items->where('type', 'Sale')->first();
        $stock = $items->where('type', 'Stock')->first();
        // dd($stock);
        // dd($sale);
        // determine status of error
        // dd($stock->prev_total_stock);
        // dd($stock->prev_total_stock != $stock->total_stock + $sale->total_stock);
        // if ($stock->prev_total_stock != $stock->total_stock + $sale->total_stock) {
        //     $error = 'TOTALS';
        //     $items->each(function ($item, $key) use ($error) {
        //         $item->error = $item->error .', ' . $error;
        //         $item->error = str_replace('0, ', '', $item->error);
        //         $item->save();
        //     });
        // }
    }

    public static function getErrors()
    {
        return self::where('error', 'like', '%over%')->orWhere('error', 'like', '%oos%');
    }

    public function scopeSearchCode($query, $search)
    {
        return $query->where(
            function ($query) use ($search) {
                $query->where('code', 'LIKE', "%$search%");
                $query->orWhere('ataco_code', 'LIKE', "%$search%");
            }
        );
    }

    public function scopeSearchDate($query, $search)
    {
        return $query->where(
            function ($query) use ($search) {
                $query->where('date', 'LIKE', "%$search%");
            }
        );
    }

    public function scopeSearchYear($query, $search)
    {
        return $query->where(
            function ($query) use ($search) {
                $query->where('year', 'LIKE', "%$search%");
            }
        );
    }

    public function scopeSearchMonth($query, $search)
    {
        $months = explode(',', $search);
        if (count($months) == 1) {
            $search = $months[0];
            return $query->where(
                function ($query) use ($search) {
                    $query->where('month', '=', "$search");
                }
            );
        # code...
        } else {
            // dd("multiple search");
            return $query->where(
                function ($query) use ($months) {
                    $query->whereBetween('month', $months);
                }
            );
        }
    }

    public function scopeSearchType($query, $search)
    {
        return $query->where(
            function ($query) use ($search) {
                $query->where('type', 'LIKE', "%$search%");
            }
        );
    }

    public function scopeSearchError($query, $search)
    {
        return $query->where(
            function ($query) use ($search) {
                $query->where('error', 'LIKE', "%$search%");
            }
        );
    }

    public static function reCheck($code=null)
    {
        if ($code == null) {
            $stockSales = self::all();
        } else {
            $stockSales = self::where('ataco_code', '=', $code)->get();
            // dd($stockSales->toArray());
        }
        $stockSales->each(function ($item) {
            $item->code ='none';
            $item->product_category ='none';
            $item->product_brand = null;
            $item->product_id = null;
            $item->save();
            echo('.');
        });
    }

    public function scopeSales($query, $currentYear, $month = null)
    {
        return $query->where(
            function ($query) use ($currentYear, $month) {
                $query->where('type', '=', "sale");
                $query->where('year', '=', $currentYear);
                if ($month != null) {
                    $query->where('month', '=', $month);
                }
            }
        );
    }

    public function scopeOtpis($query, $currentYear, $month = null)
    {
        return $query->where(
            function ($query) use ($currentYear, $month) {
                $query->where('type', '=', "Otpis");
                $query->where('year', '=', $currentYear);
                if ($month != null) {
                    $query->where('month', '=', $month);
                }
            }
        );
    }

    public function scopeType($query, $currentYear, $type, $month = null, $date = null)
    {
        return $query->where(
            function ($query) use ($currentYear, $type, $month, $date) {
                $query->where('type', '=', $type);
                $query->where('year', '=', $currentYear);
                if ($month != null) {
                    $query->where('month', '=', $month);
                }
                if ($date != null) {
                    $query->where('date', '=', $date);
                }
            }
        );
    }
}
