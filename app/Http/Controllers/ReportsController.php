<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock_Sale;
use App\Stockpile;
use App\Charts\Stock_Chart;
use App\Charts\Stacked_Chart;
use Carbon\Carbon;
use Barryvdh\Debugbar\DataCollector\ViewCollector;

class ReportsController extends Controller
{
    public function index()
    {
        $years = Stockpile::all()->groupBy('year')->keys()->toArray();

        if (request('year')) {
            $currentYear = request('year');
        } else {
            # code...
            $currentYear = Stockpile::orderBy('id', 'desc')->first()->year;
        }

        $initial_stock = Stock_Sale::Type($currentYear, 'initial')
        ->get();

        $initial_stock_next = Stock_Sale::Type($currentYear+1, 'initial')
        ->get();

        $initial_stock_next = ($initial_stock_next->count() == 0) ? 0 : $initial_stock_next->sum('total_stock') ;


        // dd($initial_stock->sum('total_stock'));

        $sales = Stock_Sale::Type($currentYear, 'sale')->get();
        $salesTotal = $sales->sum('total_stock');


        $stock = Stockpile::Stock($currentYear, 'product')
        ->get();
        $stockTotal = $stock->sum('amount');



        $otpis = Stock_Sale::Type($currentYear, 'otpis')
        ->get();
        $otpisTotal = $otpis->sum('total_stock');


        $reklamni = Stockpile::Stock($currentYear, 'reklamni')
        ->get();
        $reklamniTotal = $reklamni->sum('amount');



        // $years = $stocksales->pluck('year')->unique();

        //create chart
        $chart = new Stock_Chart;
        $chart->labels(['stock', 'sale', 'Otpis'])->options([
                'legend'=>[
                    'display' => true
                ],
                'title'=>[
                    'display' => true
                ],
                'animations'=>[
                    'easing' => 'easeInCubic',
                ],
            ]);
        ;
        $chart->dataset("Stock vs Sales {$currentYear}", 'bar', [$stockTotal, $salesTotal, $otpisTotal])->options([
            'borderColor'   =>  '#3B5C5A',
            'backgroundColor' => [
                '#06B2D6',
                '#72D771',
             ]
        ]);



        $sales_month = $sales->groupBy('month')->map(function ($row) {
            return $row->sum('total_stock');
        });

        $sales_month_neg = $sales->groupBy('month')->map(function ($row) {
            return $row->sum('total_stock')*-1;
        });
        $stock_month = $stock->groupBy('month')->map(function ($row) {
            return $row->sum('amount');
        });



        $otpis_month = $otpis->groupBy('month')->map(function ($row, $key) {
            return $row->sum('total_stock');
        });
        $reklamni_month = $reklamni->groupBy('month')->map(function ($row, $key) {
            return $row->sum('amount');
        });
        // dd($reklamni_month);

        $sales_month= $sales_month->flatten()->all();
        $stock_month= $stock_month->flatten()->all();
        $otpis_month= $otpis_month->toArray();
        $reklamni_month= $reklamni_month->toArray();

        foreach ($otpis_month as $key => $item) {
            $otpis_month[$key-1] = $item;
            unset($otpis_month[$key]);
        }

        foreach ($reklamni_month as $key => $item) {
            $reklamni_month[$key-1] = $item;
            unset($reklamni_month[$key]);
        }
        // array_pop($otpis_month);

        // dump($otpis_month);
        $arr = [0=>0,1=>0,2=>0,3=>0,4=>0,5=>0, 6=>0, 7=>0, 8=>0, 9=>0, 10=>0, 11=>0];
        $otpis_month = array_replace($arr, $otpis_month);
        $reklamni_month = array_replace($arr, $reklamni_month);
        // dd($reklamni_month);


        // dd($sales_month);
        //create chart
        $stock_sale_months = new Stock_Chart;
        $stock_sale_months->labels([1, 2, 3, 4, 5, 6, 7 ,8, 9, 10, 11, 12])->options([
                        'legend'=>[
                            'display' => true
                        ],
                        'title'=>[
                            'display' => true,
                            'text'=> "Stock vs Sales per month in {$currentYear}"
                        ],
                        'animations'=>[
                            'easing' => 'easeInCubic',
                        ],

                    ]);
        $stock_sale_months->dataset("Stock / months  {$currentYear}", 'bar', $stock_month)->options([
            'borderColor'   =>  '#3B5C5A',
            'backgroundColor' =>
            // '#72D771',
            '#06B2D6',

            ]);

        $stock_sale_months->dataset("Sales / months {$currentYear}", 'bar', $sales_month)->options([
                        'borderColor'   =>  '#3B5C5A',
                        'backgroundColor' =>
                            '#72D771',
                            // '#23E1BD',

                    ]);

        $stock_sale_months->dataset("Otpis / months {$currentYear}", 'bar', $otpis_month)->options([
                        'borderColor'   =>  '#3B5C5A',
                        'backgroundColor' =>
                            '#FF5E4D',
                            // '#23E1BD',

                    ]);

        $stock_sale_months->dataset("Reklamni / months {$currentYear}", 'bar', $reklamni_month)->options([
                        'borderColor'   =>  '#3B5C5A',
                        'backgroundColor' =>
                            '#D98014',
                            // '#23E1BD',

                    ]);
        // dd($sales_month->toArray());z

        $data['sale'] = $salesTotal;
        $data['stock'] = $stockTotal;
        $data['otpis'] = $otpisTotal;
        $data['reklamni'] = $reklamniTotal;


        // $stockpile_running->dataset("Otpis / months {$currentYear}", 'bar', $otpis_month)->options([
        //     'borderColor'   =>  '#3B5C5A',
        //     'backgroundColor' =>
        //         '#FF5E4D',
        //         // '#23E1BD',

        // ]);


        // dd($data);



        return view('reports.index', compact('chart', 'stock_sale_months', 'currentYear', 'data', 'sales_month', 'stock_month', 'otpis_month', 'reklamni_month', 'years', 'initial_stock', 'initial_stock_next', 'stockpile_running'));
    }

    public function running()
    {
        if (request('year')) {
            $currentYear = request('year');
        } else {
            # code...
            $currentYear = Carbon::now()->year;
        }

        $sales = Stock_Sale::Type($currentYear, 'sale')->get();
        $salesTotal = $sales->sum('total_stock');


        $stock = Stockpile::Stock($currentYear, 'product')
        ->get();
        $stockTotal = $stock->sum('amount');

        $years = [2016, 2017, 2018, 2019, 2020];



        $initial_stock = Stock_Sale::Type($currentYear, 'initial')
        ->get();


        $sales_month = $sales->groupBy('month')->map(function ($row) {
            return $row->sum('total_stock');
        });

        $stock_month = $stock->groupBy('month')->map(function ($row) {
            return $row->sum('amount');
        });

        // dd($reklamni_month);

        $sales_month= $sales_month->flatten()->all();
        $stock_month= $stock_month->flatten()->all();



        $stock_month_running =[];

        for ($i=1; $i < 13 ; $i++) {
            $stock_month_running[] = Stockpile::runningTotal($initial_stock->sum('total_stock'), $currentYear, $i);
        }

        $data['sale'] = $salesTotal;
        $data['stock'] = $stockTotal;

        $stockpile_running = new Stacked_Chart;
        $stockpile_running->options([
            'title'=>['text'=>'Stock vs Sales - Running total'],
            'xAxis'=>['categories'=>[1, 2, 3, 4, 5, 6, 7 ,8, 9, 10, 11, 12]],
            'yAxis'=>[
                'stackLabels'=>[
                    'enabled'=>true,
                    'style'=>[
                        'fontWeight'=>'bold',
                        'color'=> '#000000'
                    ]

                ]
            ],

            'plotOptions'=>[
                'column'=>['stacking'=>'normal'],
                ],
        ]);


        $stockpile_running->dataset("Stock", 'column', $stock_month)->options([
            'dataLabels'=>[
                'enabled'=>true,
                'allowOverlap'=>true,
                'padding'=>0,
                'color'=>'#FFFFFF',
                // 'inside'=>true,
                // 'crop'=>false,
                // 'overflow'=>'none',
            ],

        ]);
        $stockpile_running->dataset("Running Stock {$currentYear}", 'column', $stock_month_running)->options([
            'dataLabels'=>[
                'enabled'=>true,
                'allowOverlap'=>true,
                'padding'=>0,
                'color'=>'#FFFFFF',
                // 'inside'=>true,
                // 'crop'=>false,
                // 'overflow'=>'none',
            ],
            'stack'=>'stock'

        ]);
        $stockpile_running->dataset("Sales / months {$currentYear}", 'column', $sales_month)->options([
            'dataLabels'=>[
                'enabled'=>true,
                'allowOverlap'=>false,
                'padding'=>10,
                'color'=>'#FFFFFF',
                'inside'=>true,
                'crop'=>false,
                'overflow'=>'none',
            ],
            'stack'=>'stock'
        ]);

        return view('reports.running', compact('chart', 'stock_sale_months', 'currentYear', 'data', 'sales_month', 'stock_month', 'years', 'initial_stock', 'initial_stock_next', 'stockpile_running'));
        # code...
    }

    public function pie()
    {
        $year = (request('year')==null) ? 2017 : request('year');
        $month = (request('month')==null) ? 1 : request('month');
        $date = (request('date')==null) ? null : request('date');

        $stock = Stock_Sale::Type($year, 'stock', $month, $date)->get();
        $sales = Stock_Sale::Type($year, 'sale', $month, $date)->get();
        $stock_date = $stock->groupBy(function ($date) {
            return \Carbon\Carbon::parse($date->date)->format('Y-m-d');
        });
        // dd(Carbon::createFromFormat('d-m-y', '02-01-17'));
        // dd($stock_date);
        $date_keys = $stock_date->keys();
        // dump($date_keys);

        if ($date != null) {
            $stock_regions['sbrijeg'] = $stock->sum('sbrijeg');
            $stock_regions['bihac'] = $stock->sum('bihac');
            $stock_regions['bihac'] = $stock->sum('bihac');
            $stock_regions['tuzla'] = $stock->sum('tuzla');
            $stock_regions['tuzla'] = $stock->sum('tuzla');
            $stock_regions['sarajevo'] = $stock->sum('sarajevo');
            $stock_regions['sarajevo'] = $stock->sum('sarajevo');
            $stock_regions['laktasi'] = $stock->sum('laktasi');
            $stock_regions['laktasi'] = $stock->sum('laktasi');
            $stock_regions['total_stock'] = $stock->sum('total_stock');


            $sales_regions['sbrijeg'] = $sales->sum('sbrijeg');
            $sales_regions['bihac'] = $sales->sum('bihac');
            $sales_regions['bihac'] = $sales->sum('bihac');
            $sales_regions['tuzla'] = $sales->sum('tuzla');
            $sales_regions['tuzla'] = $sales->sum('tuzla');
            $sales_regions['sarajevo'] = $sales->sum('sarajevo');
            $sales_regions['sarajevo'] = $sales->sum('sarajevo');
            $sales_regions['laktasi'] = $sales->sum('laktasi');
            $sales_regions['laktasi'] = $sales->sum('laktasi');
            $sales_regions['total_stock'] = $sales->sum('total_stock');
            // dd(($stock_regions));
            // dd(array_values($stock_regions));


            $stock_regions_values = [];
            $sales_regions_values = [];
            // $salesTotal = $sales->sum('total_stock');
            foreach ($stock_regions as $key => $value) {
                $stock_regions_values[] = $value;
            }
            foreach ($sales_regions as $key => $value) {
                $sales_regions_values[] = $value;
            }



            $stock_regions_values_a = $stock_regions_values;
            unset($stock_regions_values_a[5]);

            $sales_regions_values_a = $sales_regions_values;
            unset($sales_regions_values_a[5]);
            // dd($stock_regions_values);

            $stock_regions_names = $stock_regions;
            unset($stock_regions_names['total_stock']);

            $pie_stock = new Stacked_Chart;
            $pie_stock->labels(array_keys($stock_regions_names))->options([
                'title'=>['text'=>'Stocks']
            ]);


            // $pie_stock->dataset("Stock", 'pie', )->options([
            //     'dataLabels'=>[
            //         'enabled'=>true,
            //         'allowOverlap'=>true,
            //         'padding'=>0,
            //         'color'=>'#FFFFFF',
            //         // 'inside'=>true,
            //         // 'crop'=>false,
            //         // 'overflow'=>'none',
            //     ],

            // ]);
            $pie_stock->dataset("Stock", 'pie', $stock_regions_values_a)->options([
                'borderColor'   =>  '#3B5C5A',
                'color' => [
                    '#3C989E',
                    '#0052FF',
                    '#F4CDA5',
                    '#5D81A4',
                    '#3C98FF',
                    '#84B7E8',

                ],
                'dataLabels'=>[
                            'enabled'=>true,
                            'format' => '<b>{point.name}</b>: {point.y}'

                        ],
                ]);



            $pie_sale = new Stacked_Chart;
            $pie_sale->labels(array_keys($stock_regions_names))->options([
                'title'=>['text'=>'Sales']
            ]);

            $pie_sale->dataset("Sales", 'pie', $sales_regions_values_a)->options([
                    'borderColor'   =>  '#3B5C5A',
                    'color' => [
                        '#3C989E',
                        '#0052FF',
                        '#F4CDA5',
                        '#5D81A4',
                        '#3C98FF',
                        '#84B7E8',

                    ],
                    'dataLabels'=>[
                                'enabled'=>true,
                                'format' => '<b>{point.name}</b>: {point.y}'

                            ],
                    ]);
            // $pie_stock_Sale->dataset("Sales", 'pie', $sales_regions)->options([
        //     'dataLabels'=>[
        //         'enabled'=>true,
        //         'allowOverlap'=>false,
        //         'padding'=>10,
        //         'color'=>'#FFFFFF',
        //         'inside'=>true,
        //         'crop'=>false,
        //         'overflow'=>'none',
        //     ],
        //     'stack'=>'stock'
        // ]);
        }

        return view('reports.pie', compact('date_keys', 'stock_regions', 'sales_regions', 'pie_stock', 'pie_sale'));
    }
}
