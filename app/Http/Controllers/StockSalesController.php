<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Stock_Sale;
use App\Stock;
use App\Product;
use Illuminate\Support\Facades\DB;
use File;
use Search;
use App\HiPP\Exports\Export;
use Illuminate\Support\Collection;

class StockSalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return \Excel::download(new Export("Stock_Sale"), 'stocksales.xlsx');
        // return Search::posts();
        $stockSales = Stock_Sale::getErrors()->paginate(100);

        if (request('q')) {
            // dd(request()->all());
            $stockSales = Search::stocksale(request())->paginate(100);
            // return \Excel::download(new Export(), 'stocksales.xlsx');
            // dd($stockSales);

            if (request('excel')) {
                // dd(request()->all());
                return (new Export)->forQ(Search::stocksale(request()))->download('stocksales.xlsx');
                return \Excel::download(new Export("Stock_Sale"), 'stocksales.xlsx');
            }
        }

        if (request()->segment(2)=='json') {
            // File::put(public_path("stocksales.json"), $stockSales->toJson());
            return(Stock_Sale::all()->toJson());
        }

        if (request()->segment(2)=='check') {
            Stock_Sale::checkForErrors();
        }

        if (request()->segment(2)=='all') {
            $stockSales = Stock_Sale::where('type', 'like', '%Stock%')->orWhere('type', 'like', '%sale%')->orderBy('code')->paginate(500);
            ;
            return view('stock_sale.index_all', ['stockSales'=>$stockSales]);
            // dd($stockSales);
        }

        return view('stock_sale.index', ['stockSales'=>$stockSales]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($data=null)
    {
        if ($data == null) {
            $stocksales = request()->all();
        } else {
            $stocksales = $data->toArray();
        }

        DB::transaction(function () use ($stocksales) {
            // dd($stocksales);
            if (array_key_exists(0, $stocksales)) {
                foreach ($stocksales as $entry) {
                    // dd($entry);
                    Stock_Sale::create($entry);
                    // Stock::createSale($entry);
                }
            } else {
                // dd($stocksales['type']);
                Stock_Sale::create($stocksales);
                // Stock::createSale($stocksales);
            }
        });
        return redirect("/stocksales");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $csv = Stock_Sale::getStockSalesFromText($request->text);
        return $this->create($csv);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=null)
    {
        if ($id == null) {
            return view('stock_sale.create');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
