<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charts\Stock_Chart;
use Illuminate\Support\Facades\DB;

class ChartsController extends Controller
{
    public function response()
    {
        // dd(request()->all());
        $dataset = [
            'name'  => 'Products',
            'type'  => 'bar',
            'values'=> [11,2,3,11,21,33,9],
        ];
        // $dset = DB::select('select data from data_sets where id = ?', [request('dset')]);

        // dd(($dset[0]));
        // dd(unserialize($dset[0]->data));

        // $data = unserialize($dset[0]->data);
        // $data = $dataset;
        // dd(explode(',', request('values')));
        // $values = explode(',', request('values'));
        $chart = new Stock_Chart;
        $chart->dataset('Sample Test', 'bar', [3,4,1])->color('#00ff00');
        $chart->dataset('Sample Test', 'line', [1,4,3])->color('#ff0000');
        // $chart->dataset(request('name'), request('type'), [1,2,3])->color('#00ff00');

        return $chart->api();
    }
}
