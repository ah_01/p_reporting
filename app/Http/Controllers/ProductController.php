<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Charts\Stock_Chart;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataset = [
            'name'  => 'Products',
            'type'  => 'bar',
            'values'=> [11,2,3,100,21,33,9],
        ];
        // $dset = DB::select('select data from data_sets where id = ?', [1]);

        // dd(($dset[0]));
        // dd(unserialize($dset[0]->data));

        // $data = unserialize($dset[0]->data);
        // DB::insert('insert into data_sets (data) value (?)', [serialize($dataset)]);

        $products = Product::all();
        $labels = $products->groupBy('category');
        $labelsCount = $labels->map(function ($item, $key) {
            return collect($item)->count();
        });
        // dd($labels->sort()->keys());

        // dd(implode(",", $labelsCount->sort()->toArray()));
        // dd(array_flatten($labelsCount->sort()->toArray()));
        $chart = new Stock_Chart;
        $api = url('/test_data');
        // dd($api);
        // $chart->dataset('Sample', 'bar', [100, 65, 84, 45, 90])
        // ->options([
        //     // 'borderColor'   =>  '#36a2eb',
        //     // 'color' => [
        //     //     '#ff6384',
        //     //     '#36a2eb',
        //     //     '#cc65fe',
        //     // ]
        // ]);
        $chart->labels([1,2,3,4,5,6,7])->load($api);
        return view("product.index", ['products'=>$products, 'chart'=>$chart]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (request()->isMethod('POST')) {
            $p = Product::create(request()->all());
            return redirect("product/$p->id");
        }
        return view("product.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Product::addProductsFromText($request->text);
        return redirect("/product");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $id)
    {
        return view('product.show', ['product'=>$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $id)
    {
        $id->fill($request->all());
        $id->save();
        return redirect("/product/$id->id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function stranica(Product $id)
    {
        $id->stranica();
        return back();
    }
}
