<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock_Sale;

class SearchController extends Controller
{
    public function index()
    {
        return view('scan');
    }
    //
    public function show()
    {
        $search = request('q');
        return Stock_Sale::search($search)->paginate(19);
    }
}
