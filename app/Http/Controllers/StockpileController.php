<?php
namespace App\Http\Controllers;

use Search;
use App\Stockpile;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\HiPP\Exports\Export;
use App\Charts\Stock_Chart;

class StockpileController extends Controller
{
    public function index()
    {
        if (request('q')) {
            // dd("search results");

            $stockpile = Search::stockpile(request())->get();


            if (request('excel')) {
                // dd(request()->all());
                return (new Export)->forQ(Search::stockpile(request()))->download('stockpile.xlsx');
                return \Excel::download(new Export("Stockpile"), 'stockpile.xlsx');
            }

            $chart = new Stock_Chart;
            return view('stockpile.index_list', compact(['stockpile','total', 'chart' ]));
        }


        $stockpile = Stockpile::all()->groupBy('code');
        return view('stockpile.index', compact('stockpile'));
    }

    public function create($data = null)
    {
        if ($data == null) {
            $stockpile = request()->all();
        } else {
            $stockpile = $data->toArray();
        }
        // dd($stockpile);

        DB::transaction(function () use ($stockpile) {
            // dd($stockpile);
            if (array_key_exists(0, $stockpile)) {
                foreach ($stockpile as $entry) {
                    $entry['price'] = Stockpile::getNumVal($entry['price']);
                    // $entry['comment'] = ($entry['comment'] == null) ? 'nan' : $entry['comment'] ;
                    // dump($entry["comment"]);
                    Stockpile::create($entry);
                }
            } else {
                // dd($stockpile['type']);
                Stockpile::create($stockpile);
            }
        });
        return redirect("/stockpile");
    }

    public function show()
    {
        return view('stockpile.create');
    }

    public function store()
    {
        // dd(request('text'));
        $csv = Stockpile::getFromText(request('text'));
        return $this->create($csv);
    }

    public function product($s)
    {
        $stockpile = Stockpile::where('code', '=', $s)->get();
        $total = Stockpile::getTotal($s);
        $chart = new Stock_Chart;

        // dd($stockpile);
        return view('stockpile.index_list', compact(['stockpile','total', 'chart' ]));
    }
}
