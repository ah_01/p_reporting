<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Invoice extends Model
{
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['delivery_date'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            $model->products()->each(function ($product) {
                $product->delete();
            });
        });
    }



    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot(
            'id',
            'delivery_date',
            'product_id',
            'code',
            'pcs',
            'price_unit',
            'total_price',
            'expiry_date',
            'type',
            'product_total'

        );
    }

    public static function getProductInfoFromText($text)
    {
        //TODO: Extract csv creation logic to helper
        // Extract csv creation logic to helper
        $csv = collect(explode("\n", $text));
        $csv = $csv->map(function ($item) {
            return str_getcsv($item, "\t");
        });
        $fields = $csv->shift();

        $csv = $csv->map(function ($item) use ($fields) {
            $i=0;
            $p=[];
            foreach ($item as $key => $value) {
                // in case we are looking in code field
                if ($i==0) {
                    // dump($value);
                    // dump(explode('-', $value, 2)[0]);
                    $code = explode('-', $value, 2);
                    // dump($code[0]);
                    // dump(Product::where('code', $code[0])->get()->toArray());
                    $value = Product::where('code', $code[0])->exists() ? $value : 'NONE-' . $value ;
                }
                //clean second andthird fields
                if ($i >= 2 and $i <= 4) {
                    $value = str_replace(['.',',00'], '', $value);
                    $value = str_replace([','], '.', $value);
                    // $value = $value * 100;
                }
                $p[$fields[$i]]=$value;
                $i++;
            }
            // dd($p);
            return $p;
        });
        //TODOEND:  --------------------------------------------------------------------

        // check if product does not exists - if not made new collection fo structur NONE-CODE
        $check = $csv->groupBy('code')->keys()->filter(function ($value, $key) {
            // dump($value);
            return starts_with($value, 'NONE-');
        });
        // if check count is grateer than 0 throw exception with list of nonexistent codes
        if ($check->count()>0) {
            throw new \Exception("Product does not exists!" . $check, 1);
        }

        return $csv;
    }

    public function storeProductsFromCsv($csv)
    {
        $csv->each(function ($item) {
            $code = explode('-', $item['code'], 2)[0];
            $product = Product::where('code', $code)->firstOrFail();

            //Delivery date is invoice date not truck date
            $item['delivery_date'] = $this->delivery_date;

            // Every invoice added is of type sttock
            $item['type'] = 'Stock';

            array_forget($item, 'title');
            $this->storeProduct($product, $item);
        });
    }


    public function runningTotalPcs(Product $p)
    {
        // TODO: product_total change to product_running_total

        //get all invoices
        //do summ of all products pieces per invoice
        // summ all

        // dump($p);
        $invoices = self::all();

        $product_running_total = 0;

        $invoices->each(function ($item, $key) use ($p, &$product_running_total) {
            $product_running_total += $item->getProductGroup($p)->sum('pivot.pcs');
        });

        return $product_running_total;
    }

    public function storeProduct($product, $attributes)
    {
        $attributes['total_price'] = (array_has($attributes, 'total_price')) ? $attributes['total_price'] : $attributes['pcs'] * $attributes['price_unit'] ;

        $attributes['product_total'] = $this->runningTotalPcs($product) + $attributes['pcs'];

        // dump($attributes);

        return $this->products()->attach($product->id, $attributes);
    }

    public function productTotalPcs(Product $p)
    {
        //extract to new entity
        // dd($this->products()->get()->groupBy('code')->get($p->code)->sum('pivot.pcs'));

        $value = $this->products()->get()->groupBy('code')->get($p->code)->sum('pivot.pcs');

        return $value;
    }

    public function getProductGroup($p)
    {
        $products = $this->products()->where('product_id', $p->id)->get();
        return $products;
    }
}
