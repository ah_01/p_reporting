<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $guarded =[
        // 'ataco_code',
        'kat_num',
        'opis',
        'prev_total_stock',
        'shipment',
        'sbrijeg',
        'bihac',
        'tuzla',
        'sarajevo',
        'laktasi',
        'total_stock',
        // 'date',
        // 'type',



    ];
    protected $dates = ['date'];

    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $p = Product::where('ataco_code', $model->ataco_code)->firstOrFail();
            // dump($model->toArray());

            $model->code = $p->code;
            $model->year = $model->date->year;
            $model->month = $model->date->month;
            // dd($model->toArray());
        });
    }

    public static function createSale($entry)
    {
        if ($entry['type']=="Sale") {
            $entry['amount'] = $entry['total_stock'] * -1;
            return self::create($entry);
        }
        // dd('No Stock added!');
    }
}
