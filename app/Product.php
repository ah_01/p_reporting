<?php

namespace App;

use App\Stock_Sale;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    // protected $fillable = [
    //     'ataco_code',
    //     'code',
    //     'name_eng',
    //     'category',
    //     'brand',
    //     'web',
    // ];
    protected $guarded = [];

    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            // $p = Product::where('ataco_code', $model->ataco_code)->firstOrFail();
            $code = explode('-', $model->code, 2);
            // dd($code);
            $model->code = $code[0];
            if (count($code) == 2) {
                $model->code_version = $code[1];
            }
        });
    }


    public static function addProductsFromText($text)
    {
        //TODO:   Extract csv creation logic to helper
        // Extract csv creation logic to helper
        $csv = collect(explode("\n", $text));
        $csv = $csv->map(function ($item) {
            return str_getcsv($item, "\t");
        });
        $fields = $csv->shift();

        // dd($fields);

        $csv = $csv->map(function ($item) use ($fields) {
            $i=0;
            $p=[];
            foreach ($item as $key => $value) {
                $p[$fields[$i]]=$value;
                $i++;
            }
            // dd($p);
            return $p;
        });
        //TODOEND:  --------------------------------------------------------------------
        // dd($csv);
        DB::transaction(function () use ($csv) {
            $csv->each(function ($item) {
                self::createProduct($item);
            });
        });

        // return $csv;
    }
    public static function createProduct($array)
    {
        $p = Product::create($array);
        return $p->save();
        dd($p->save());
    }

    public function Invoices()
    {
        return $this->belongsToMany('App\Invoice');
    }

    public function stocks_sales()
    {
        return $this->hasMany(Stock_Sale::class);
    }

    public function stranica()
    {
        $this->stranica = 1 - $this->stranica;
        $this->save();
    }
}
