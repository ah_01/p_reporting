<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $dateFormat = 'Y-m-d';
    protected $dates = ['expense_date'];


    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->record_unique = $model->ataco_code . '_' . $model->expense_date->toDateString() . '_' . $model->type;
        });
    }
}
