<?php

use Faker\Generator as Faker;
use App\Stock;
use App\Product;

$factory->define(Stock::class, function (Faker $faker) {
    return [
        'ataco_code' => $ataco_code = factory(Product::class)->create()->ataco_code,
        'code' => $year = 'HR' . $ataco_code,
        'amount' => $ataco_code = $faker->randomElement([100, 200, 300, 400, 500]),
        'type' => $ataco_code = $faker->randomElement(['Stock', 'Sale']),
        'date' =>$faker->dateTimeBetween('-6 months'),
    ];
});
