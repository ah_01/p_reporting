<?php

use Faker\Generator as Faker;
use App\Product;
use Carbon\Carbon;

$factory->define(App\Stockpile::class, function (Faker $faker) {
    return [
        'ataco_code' => $ataco_code = factory(Product::class)->create()->ataco_code,
        'code' => $year = 'HR' . $ataco_code,
        'title' => $faker->sentence(),
        'amount' => $ataco_code = $faker->randomElement([100, 200, 300, 400, 500]),
        'type' => $ataco_code = $faker->randomElement(['product', 'advertising']),
        'date' =>"11.04.2018",
        'exp_date' =>"11.04.2020",
        'truck' =>$faker->numberBetween(1, 10),
        'price' =>$faker->numberBetween(1, 10),
        'total' =>$faker->numberBetween(100, 500),
    ];
});
