<?php

use Faker\Generator as Faker;
use App\Stock_Sale as StockSale;
use App\Product;

$factory->define(StockSale::class, function (Faker $faker) {
    return [
        'ataco_code'=>function () {
            return factory(Product::class)->create()->ataco_code;
        },
        'kat_num'=>$faker->unique()->numberBetween(4000, 4500),
        'opis'=>$faker->sentence(),
        'prev_total_stock'=>$faker->randomElement([100, 200, 300, 400, 500]),
        'shipment'=>$faker->randomElement([100, 200, 300, 400, 500]),
        'sbrijeg'=>$faker->randomElement([100, 200, 300, 400, 500]),
        'bihac'=>$faker->randomElement([100, 200, 300, 400, 500]),
        'tuzla'=>$faker->randomElement([100, 200, 300, 400, 500]),
        'sarajevo'=>$faker->randomElement([100, 200, 300, 400, 500]),
        'laktasi'=>$faker->randomElement([100, 200, 300, 400, 500]),
        'total_stock'=>$faker->randomElement([100, 200, 300, 400, 500]),
        'date'=>$faker->dateTimeBetween('-6 months', 'now'),
        'type'=>$faker->randomElement(['stock', 'sale']),
    ];
});
