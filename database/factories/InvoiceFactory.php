<?php

use Faker\Generator as Faker;
use App\Invoice;

$factory->define(Invoice::class, function (Faker $faker) {
    return [
        'invoice_number'=>$faker->unique()->randomNumber(),
        'delivery_date'=>$faker->dateTimeThisMonth(),
    ];
});
