<?php

use Faker\Generator as Faker;
use App\Product;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'code' => $faker->unique()->numberBetween(1000, 1500),
        'category' => $product = $faker->randomElement(['milk', 'juice', 'fruits', 'pouch']),
        'name_eng' => $product . $code = $faker->unique()->numberBetween($min = 7000, $max = 7200),
        'name_ba' => $product . $code,
        'brand' => $faker->randomElement(['HiPP', 'Bebivita']),
        'country' => $faker->randomElement(['Bosnia', 'Turkiye']),
        'comment' => $faker->text(),


        'ataco_code' => $faker->unique()->numberBetween(10000, 10500),

        'em_no' => "EM_No:". $code,
        'status' => $faker->randomElement($array = array('active','delisted')),
        'price' => $faker->randomElement(['1000', '1500', '1700']),
        'type' => $faker->randomElement(['gratis', 'regular']),
    ];
});
