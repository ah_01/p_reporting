<?php

use Faker\Generator as Faker;
use App\Truck;

$factory->define(Truck::class, function (Faker $faker) {
    return [
        'number' => $num = $faker->unique()->randomNumber(),
        'year' => $year = $faker->year,
        'number_year' => $num . '_' . $year,
        'delivery_date' =>$faker->dateTimeBetween('-1 year'),
        'invoice_number' => $faker->randomNumber(),
        

    ];
});
