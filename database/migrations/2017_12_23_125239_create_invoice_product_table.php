<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_product', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('invoice_id')->unsigned();
            $table->integer('product_id')->unsigned();

            $table->date('delivery_date');

            $table->string('code');
            $table->date('expiry_date');
            $table->integer('pcs');
            $table->decimal('price_unit'); //TODO: price and total price formatting of field while retrieveing information, maybe will be excluded by decimal
            $table->decimal('total_price');
            $table->integer('product_total')->default(0);
            $table->string('type'); //value: "Stock" - all invoices are stock value , advertising material should be different



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_product');
    }
}
