<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockAndSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks_and_sales', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id');

            $table->string('ataco_code');
            $table->integer('kat_num')->nullable();
            $table->string('opis');
            $table->integer('prev_total_stock')->nullable();
            $table->integer('shipment')->nullable();
            $table->integer('sbrijeg');
            $table->integer('bihac');
            $table->integer('tuzla');
            $table->integer('sarajevo');
            $table->integer('laktasi');
            $table->integer('total_stock');

            $table->string('type');

            $table->date('date');
            //auto generated fields - boot function on model
            $table->string('code');
            $table->string('product_category');
            $table->string('product_brand');
            $table->integer('product_id');
            // $table->string('acode_date_type')->storedAs("CONCAT(ataco_code,'_',date,'_',type )")->unique();
            $table->string('acode_date_type')->unique(); //TODO: exchange this line with uppper after testing is complete - e.g. on production server
            $table->integer('month');
            $table->integer('year');
            $table->string('error')->default("0");
            // $table->string('average')->storedAs((sbrijeg+bihac+tuzla+sarajevo+laktasi)/3);
            $table->integer('average');//TODO: exchange this line with uppper after testing is complete - e.g. on production server



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_and_sales');
    }
}
