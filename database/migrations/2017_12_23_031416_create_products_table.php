<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name_eng');
            $table->string('name_ba')->nullable();
            $table->string('country')->nullable();
            $table->string('code')->unique();
            $table->integer('ataco_code')->unique();
            $table->string('brand')->nullable();
            $table->string('category')->nullable();
            $table->string('em_no')->nullable();
            $table->string('type')->nullable();
            $table->string('status')->nullable()->default('---');
            $table->integer('price')->nullable();
            $table->text('comment')->nullable();
            $table->integer('stranica')->default(0);




            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
