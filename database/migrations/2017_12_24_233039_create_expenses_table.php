<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            // $table->primary('ataco_code', 'expense_date');

            $table->integer('ataco_code');
            $table->integer('kat_num');
            $table->string('title');
            $table->integer('amount');
            $table->string('type');
            $table->date('expense_date');

            //field with structire: expense_date_type will be created to ensure uniquenes of expense
            $table->string('record_unique')->unique();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
