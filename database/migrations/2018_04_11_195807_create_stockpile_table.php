<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockpileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stockpiles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('ataco_code')->default("NO CODE");
            $table->string('code');
            $table->string('title');
            $table->integer('amount');
            $table->integer('price');
            $table->string('total');
            $table->string('type')->default("NO TYPE");
            $table->string('date');
            $table->string('exp_date');
            $table->string('truck');
            $table->string('comment')->default('none');

            // Autogenrated fields
            $table->integer('month');
            $table->integer('year');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stockpiles');
    }
}
