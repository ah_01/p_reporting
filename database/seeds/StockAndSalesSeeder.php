<?php

use Illuminate\Database\Seeder;
use App\Stock_Sale;
use Carbon\Carbon;
use App\Product;

class StockAndSalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Product::class)->create(['ataco_code'=>1020]);
        factory(Stock_Sale::class, 1)->create([
            'ataco_code'=>1020,
            'kat_num'=>00001,
            'opis'=>'Opis',
            'prev_total_stock'=>0,
            'shipment'=>500,
            'sbrijeg'=>100,
            'bihac'=>100,
            'tuzla'=>100,
            'sarajevo'=>100,
            'laktasi'=>100,
            'total_stock'=>500,
            'date'=>Carbon::parse('2018-01-01'),
            'type'=>'Stock'
            ]);


        factory(Stock_Sale::class, 1)->create([
            'ataco_code'=>1020,
            'kat_num'=>00001,
            'opis'=>'Opis',
            'prev_total_stock'=>0,
            'shipment'=>0,
            'sbrijeg'=>50,
            'bihac'=>0,
            'tuzla'=>50,
            'sarajevo'=>100,
            'laktasi'=>0,
            'total_stock'=>0,
            'date'=>Carbon::parse('2018-01-07'),
            'type'=>'Sale'
            ]);

        factory(Stock_Sale::class, 1)->create([
            'ataco_code'=>1020,
            'kat_num'=>00001,
            'opis'=>'Opis',
            'prev_total_stock'=>300,
            'shipment'=>0,
            'sbrijeg'=>50,
            'bihac'=>100,
            'tuzla'=>50,
            'sarajevo'=>0,
            'laktasi'=>100,
            'total_stock'=>300,
            'date'=>Carbon::parse('2018-01-14'),
            'type'=>'Stock'
            ]);


        factory(Stock_Sale::class, 100)->create();
    }
}
