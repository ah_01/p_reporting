<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Product;
use App\Stock_Sale;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Carbon;
use App\Stock;
use App\User;

class CurrentStockFeatureTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    //TODO: Test disabled autimatic creation of stock is disabled
    /** @test */
    // public function adding_sales_creates_current_stock_entry()
    // {
    //     $this->disableExceptionHandling();
    //     $this->assertEquals(0, Stock_Sale::count());


    //     $response = $this->actingAs($this->user)->post('/stocksales/create', [
    //         'ataco_code'=>factory(Product::class)->create()->ataco_code,
    //         'kat_num'=>4990,
    //         'opis'=>'Opis',
    //         'prev_total_stock'=>50,
    //         'shipment'=>0,
    //         'sbrijeg'=>100,
    //         'bihac'=>100,
    //         'tuzla'=>100,
    //         'sarajevo'=>100,
    //         'laktasi'=>100,
    //         'total_stock'=>500,
    //         'date'=>Carbon::parse('2018-01-01'),
    //         'type'=>'Sale'
    //         ]);

    //     $this->assertEquals(1, Product::count());
    //     $this->assertEquals(1, Stock_Sale::count());

    //     $this->assertEquals(1, Stock::count());
    //     $this->assertEquals(-500, Stock::first()->amount);
    // }
    /** @test */
    public function adding_stock_type_does_not_create_entry()
    {
        $this->disableExceptionHandling();
        $this->assertEquals(0, Stock_Sale::count());

        $response = $this->actingAs($this->user)->post('/stocksales/create', [
            'ataco_code'=>factory(Product::class)->create()->ataco_code,
            'kat_num'=>4990,
            'opis'=>'Opis',
            'prev_total_stock'=>50,
            'shipment'=>0,
            'sbrijeg'=>100,
            'bihac'=>100,
            'tuzla'=>100,
            'sarajevo'=>100,
            'laktasi'=>100,
            'total_stock'=>500,
            'date'=>Carbon::parse('2018-01-01'),
            'type'=>'Stock'
            ]);

        $this->assertEquals(1, Product::count());
        $this->assertEquals(1, Stock_Sale::count());

        $this->assertEquals(0, Stock::count());
    }

    /** @test */
    public function current_stock_has_index_view()
    {
        $this->disableExceptionHandling();
        $this->assertEquals(0, Stock::count());

        factory(Stock::class, 3)->create();

        $this->assertEquals(3, Stock::count());

        $stocks = Stock::all();

        $response = $this->actingAs($this->user)->get('/stock');
        $response->assertStatus(200);
        $response->assertViewIs('stock.index');
        $this->assertTrue($response->data('stocks')->first()->is($stocks->first()));
        $this->assertTrue($response->data('stocks')->last()->is($stocks->last()));
    }

    /** @test */
    public function current_stock_has_json_view()
    {
        $this->disableExceptionHandling();
        $this->assertEquals(0, Stock::count());
        $data = [
            'ataco_code'=> $ac = factory(Product::class)->create()->ataco_code,
            'code'=>'HR' .$ac,
            'amount'=>'100',
            'type'=>'Stock',
            'date'=>'2018-01-01',
        ];
        factory(Stock::class, 6)->create();
        factory(Stock::class)->create($data);

        $response = $this->actingAs($this->user)->get('/stock/json');

        $response->assertSuccessful();
        $response->assertStatus(200);
        $data = Stock::all()->toArray();
        $response->assertJson($data);
    }
}
