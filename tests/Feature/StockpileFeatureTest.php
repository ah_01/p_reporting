<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Stockpile;
use App\User;
use App\Product;
use App\Stock_Sale;

class StockpileFeatureTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    /** @test */
    public function stockpile_has_index_view()
    {
        $this->disableExceptionHandling();
        $this->assertEquals(0, Stockpile::count());

        factory(Stockpile::class, 3)->create();

        $this->assertEquals(3, Stockpile::count());

        $stockpile = Stockpile::all();

        $response = $this->actingAs($this->user)->get('/stockpile');
        $response->assertStatus(200);
        $response->assertViewIs('stockpile.index');

        // Obsolete test because changed structure of view
        // $this->assertTrue($response->data('stockpile')->first()->is($stockpile->first()));
        // $this->assertTrue($response->data('stockpile')->last()->is($stockpile->last()));
    }

    /** @test */
    public function stockpile_has_getTotal_function()
    {
        factory(Stockpile::class, 3)->create();
        factory(Stockpile::class)->create(['code'=>10001, 'amount'=>100]);
        factory(Stockpile::class)->create(['code'=>10001, 'amount'=>300]);

        $this->assertEquals(5, Stockpile::count());
        $this->assertEquals(400, Stockpile::getTotal(10001));
    }

    /** @test */
    public function stockpile_can_be_created_from_text()
    {
        $this->disableExceptionHandling();

        $text="code	title	amount	exp_date	price	total	truck	date	comment
AL2012-F-U	HiPP 1 Combiotic Infant Milk (p)	560	19.05.2019	3.58	2004.8	7.2018	12.04.2018	n
AL2013-01	HiPP 1 Combiotic Infant Milk (p)	900	28.05.2019	8.11	7299	7.2018	12.04.2018	n
AL2016-02-U	HiPP 1 Organic Infant Milk (p)	980	25.08.2019	3.15	3087	7.2018	12.04.2018	n";

        $stockpile = Stockpile::getFromText($text);
        // dd($stockpile->toArray());

        $this->assertEquals(3, $stockpile->count());

        // dd($stockpile->toarray());

        $response = $this->actingAs($this->user)->post("/stockpile/create", $stockpile->toarray());

        // Stockpile::saveEntries($csv);
        $this->assertEquals(3, Stockpile::count());
    }



    /** @test */
    public function stockpile_has_running_total()
    {
        // runnning_total_stockpile_month  = initial stockpile and current month stockpile
        //current_stockpile_month = running_stockpile_month  - sales_month (Sales from stocSales)

        // better:

        // running total stockpile = intial_stockpile  + Stockpile - Sales / Per month


        //create product
        factory(Product::class)->create(['ataco_code'=>10001, 'code'=>'HR10001']);

        //Add initial amount in Stock_Sales
        factory(Stock_Sale::class)->create([
            'ataco_code'=>10001,
            'kat_num'=>4900,
            'opis'=>'Opis',
            'prev_total_stock'=>100,
            'shipment'=>0,
            'sbrijeg'=>100,
            'bihac'=>100,
            'tuzla'=>100,
            'sarajevo'=>100,
            'laktasi'=>0,
            'total_stock'=>500,
            'date'=>'2018-01-01',
            'type'=>'initial',
        ]);
        $this->assertEquals(1, Stock_Sale::count());


        // check for initial amount
        $initial_Stock = Stock_Sale::Type(2018, 'initial')->get()->sum('total_stock');
        $this->assertEquals(500, $initial_Stock);


        // add stock for 1-3 months
        factory(Stockpile::class)->create(['code'=>'HR10001', 'amount'=>100, 'date'=>'2018-01-07']);
        factory(Stockpile::class)->create(['code'=>'HR10001', 'amount'=>500, 'date'=>'2018-02-07']);
        factory(Stockpile::class)->create(['code'=>'HR10001', 'amount'=>500, 'date'=>'2018-03-07']);


        $this->assertEquals(3, Stockpile::count());

        //check for stockpile for whole year
        $this->assertEquals(1100, Stockpile::Stock(2018, 'product')->get()->sum('amount'));

        //check for stockpile per month
        $this->assertEquals(100, Stockpile::Stock(2018, 'product', 1)->get()->sum('amount'));
        $this->assertEquals(500, Stockpile::Stock(2018, 'product', 2)->get()->sum('amount'));
        $this->assertEquals(500, Stockpile::Stock(2018, 'product', 3)->get()->sum('amount'));


        // check for running total amount for each month stock has added
        $running1 = Stockpile::runningTotal($initial_Stock, 2018, 1);
        $this->assertEquals(600, Stockpile::runningTotal($initial_Stock, 2018, 1));

        factory(Stock_Sale::class)->create(['ataco_code'=>10001, 'total_stock'=>200, 'date'=>'2018-01-07', 'type'=>'sale']);
        $this->assertEquals(400, Stockpile::runningTotal($initial_Stock, 2018, 1)); //400

        $this->assertEquals(900, Stockpile::runningTotal($initial_Stock, 2018, 2));
        $this->assertEquals(1400, Stockpile::runningTotal($initial_Stock, 2018, 3));

        factory(Stock_Sale::class)->create(['ataco_code'=>10001, 'total_stock'=>100, 'date'=>'2018-03-07', 'type'=>'sale']);
        $this->assertEquals(1300, Stockpile::runningTotal($initial_Stock, 2018, 3));
    }
}
