<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Truck;
use App\User;

class TruckFetaureTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function view_trucks()
    {
        $this->disableExceptionHandling();
        $user = factory(User::class)->create();
        factory(Truck::class, 10)->create();

        $response = $this->actingAs($user)->get('/truck');
        $trucks = Truck::all();

        $response->assertStatus(200);
        $response->assertViewIs('truck.index');
        $this->assertTrue($response->data('trucks')->diff($trucks)->count()==0);
    }

    /** @test */
    public function truck_can_be_created()
    {
        $this->disableExceptionHandling();
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->post('/truck/create', [
            'number'=>1,
            'year'=>1,
            'invoice_number'=>10001,
            'delivery_date'=>'2018-01-01',
        ]);

        $response->assertStatus(302);
        $this->assertEquals(1, Truck::count());
    }

    /** @test */
    public function truck_can_be_updated()
    {
        $this->disableExceptionHandling();

        $user = factory(User::class)->create();
        $truck = factory(Truck::class)->create([
                'id'=>11,
                'number'=>22,
                'year'=>2018,
                ]);

        $response = $this->actingAs($user)->get("/truck/$truck->id");

        $response->assertStatus(200);
        $response->assertSee("22");
        $response->assertSee("2018");
        $response->assertSee("22_2018");

        $response = $this->actingAs($user)->post("/truck/$truck->id", [

            'number'=>2001,

            ]);

        $response->assertStatus(302);
        $response->assertRedirect("/truck/$truck->id");

        $response = $this->actingAs($user)->get("/truck/$truck->id");
        $response->assertSee("2001");
        $response->assertSee("2001_2018");
    }


    /** @test */
    public function truck_can_be_viewed()
    {
        $this->disableExceptionHandling();
        factory(Truck::class, 10)->create();
        $user = factory(User::class)->create();


        $truck = Truck::first();
        $response = $this->actingAs($user)->get("/truck/$truck->id");

        $response->assertStatus(200);
        $response->assertViewIs('truck.show');
        $this->assertTrue($response->data('truck')->is($truck));
    }


    /** @test */
    public function truck_create_view_is_available()
    {
        $this->disableExceptionHandling();

        $user = factory(User::class)->create();


        $response = $this->actingAs($user)->get('/truck/create');

        $response->assertStatus(200);
        $response->assertViewIs('truck.create');
        $response->assertSee("Create truck");
    }
}
