<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Product;

// TODO:make assertions using data returned from controlles to decouple views from test logic - eventually!

class ProductFeatureTest extends TestCase
{
    use DatabaseMigrations;

    // view products
    /** @test */
    public function products_can_be_viewed()
    {
        $this->disableExceptionHandling();

        $user = factory(User::class)->create();
        $product = factory(Product::class)->create(['code'=>"HiPP0001"]);

        $response = $this->actingAs($user)->get('/product');

        $response->assertStatus(200);
        $response->assertViewIs('product.index');
        $response->assertSee("Products");
        $response->assertSee("HiPP0001");
        // TODO:make assertions about actual products listed !
    }

    // create product
    /** @test */
    public function product_have_create_view()
    {
        $this->disableExceptionHandling();

        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get('/product/create');

        $response->assertStatus(200);
        $response->assertViewIs('product.create');
        $response->assertSee("Create Product");
    }

    // edit update product
    /** @test */
    public function product_can_be_updated()
    {
        $this->disableExceptionHandling();

        $user = factory(User::class)->create();
        $product = Product::create([

            'code'          => "HiPP0001",
            'ataco_code'    => 1110001,
            'name_eng'      => "Hipp product",
            'name_ba'       => "Hipp proizvod",
            'country'       => "BA",
            'brand'         => "HiPP",
            'category'      => "Milk",
            'em_no'         => "11061",
            'type'          => "product",
            'status'        => "active",
            'price'         => 100,
            'comment'       => "---",
            // 'stranica'      => 0,

        ]);

        $response = $this->actingAs($user)->get("/product/$product->id");

        $response->assertStatus(200);
        $response->assertViewIs('product.show');

        $response->assertSee("1110001");
        $response->assertSee("HiPP0001");
        $response->assertSee("Hipp product");
        $response->assertSee("Hipp proizvod");
        $response->assertSee("BA");
        $response->assertSee("HiPP");
        $response->assertSee("Milk");
        $response->assertSee("11061");
        $response->assertSee("product");
        $response->assertSee("active");
        $response->assertSee("100");
        $response->assertSee("---");
        $response->assertSee("Stranica:0");

        $post = $this->actingAs($user)->post("/product/$product->id", [
            'ataco_code'=>1110003,
            'code'=>"HiPP0003",
            'name_eng'=>"HiPP Product Test",
            ]);
        $post->assertRedirect("/product/$product->id");
        $response = $this->actingAs($user)->get("/product/$product->id");

        $response->assertSee("1110003");
        $response->assertSee("HiPP0003");
        $response->assertSee("HiPP Product Test");
    }

    // add single product from text
    // add multiple products from text
    /** @test */
    public function products_can_be_added_from_text()
    {
        $this->disableExceptionHandling();
        $user = factory(User::class)->create();

        $text ="ataco_code	code	name_eng
1110001	AL0002	Test Product
1120001	HiPP0002	HiPP test";

        //
        $this->assertEquals(0, Product::count());
        $response= $this->actingAs($user)->post("product/store", ['text'=>$text]);

        $response->assertRedirect("/product");
        $this->assertEquals(2, Product::count());
    }


    /** @test */
    public function Product_has_endpoint_to_change_status()
    {
        $this->disableExceptionHandling();

        $user = factory(User::class)->create();
        factory(Product::class)->create();
        $product = Product::first();
        $this->assertEquals(1, Product::count());
        $this->assertEquals(0, $product->stranica);

        $response = $this->actingAs($user)->from("/product/$product->id")->get("/product/$product->id/stranica");
        $response->assertRedirect("/product/$product->id");

        $this->assertEquals(1, $product->fresh()->stranica);
    }
}
