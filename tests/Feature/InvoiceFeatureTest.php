<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Invoice;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Carbon\Carbon;
use App\Truck;
use App\Product;
use App\Stock_Sale;
use Illuminate\Support\Facades\DB;
use App\User;

class InvoiceFeatureTest extends TestCase
{
    use DatabaseMigrations;
    private function validParams($overrides=[])
    {
        return array_merge([
            'delivery_date'=>factory(Truck::class)->create()->delivery_date,
            'product_id' => 1,
            'code'=>1001,
            'pcs'=>0,
            'price_unit'=>1,
            'total_price'=>0,
            'product_total'=>0,
            'expiry_date'=>'2018-01-07',
            'expiry_date'=>Carbon::parse('2018-01-01')->addYear(),
            'type'=>"stock",
        ], $overrides);
    }


    /** @test */
    public function view_invoices()
    {
        $this->disableExceptionHandling();
        $user = factory(User::class)->create();
        factory(Invoice::class, 10)->create();

        $response = $this->actingAs($user)->get('/invoice');
        $invoices = Invoice::all();

        $response->assertStatus(200);
        $response->assertViewIs('invoice.index');
        $this->assertTrue($response->data('invoices')->diff($invoices)->count()==0);
    }

    /** @test */
    public function invoice_can_be_created()
    {
        $this->disableExceptionHandling();
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->post('/invoice/create', [
            'invoice_number'=>10001,
            'delivery_date'=>'2018-01-01',
        ]);

        $this->assertEquals(1, Invoice::count());
        $this->assertEquals(10001, Invoice::first()->invoice_number);
    }

    /** @test */
    public function invoice_can_be_viewed()
    {
        $this->disableExceptionHandling();
        factory(Invoice::class, 10)->create();
        $user = factory(User::class)->create();


        $invoice = Invoice::first();
        $response = $this->actingAs($user)->get("/invoice/$invoice->id");

        $response->assertStatus(200);
        $response->assertViewIs('invoice.show');
        $this->assertTrue($response->data('invoice')->is($invoice));
    }

    /** @test */
    public function invoice_create_view_is_available()
    {
        $this->disableExceptionHandling();

        $user = factory(User::class)->create();


        $response = $this->actingAs($user)->get('/invoice/create');

        $response->assertStatus(200);
        $response->assertViewIs('invoice.create');
        $response->assertSee("Create Invoice");
    }

    /** @test */
    public function invoice_and_all_products_entries_can_be_deleted()
    {
        $this->disableExceptionHandling();

        $products1 = factory(Product::class, 5)->create();
        $products2 = factory(Product::class, 3)->create();
        $invoice1 = factory(Invoice::class)->create();
        $invoice2 = factory(Invoice::class)->create();


        // $this->assertEquals(5, Product::count());

        $products1->each(function ($product) use ($invoice1) {
            $invoice1->products()->attach($product, $this->validParams(['product_id'=>$product->id]));
        });

        $products2->each(function ($product) use ($invoice2) {
            $invoice2->products()->attach($product, $this->validParams(['product_id'=>$product->id]));
        });

        $this->assertEquals(5, $invoice1->products()->count());
        $this->assertEquals(3, $invoice2->products()->count());
        $this->assertEquals(8, Product::count());


        $invoice1->delete();

        $this->assertEquals(3, Product::count());
        $this->assertEquals($products2->pluck('ataco_code'), $invoice2->products->pluck('ataco_code'));
    }
}
