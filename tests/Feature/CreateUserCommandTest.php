<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class CreateUserCommandTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function create_user_via_the_cli()
    {
        $this->assertEquals(0, User::count());
        $this->artisan('create-user', ['email' => 'test@example.com', 'password'=>'secret']);

        $this->assertEquals(1, User::count());
    }
}
