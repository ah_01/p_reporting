<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Stock_Sale;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Carbon\Carbon;
use App\Product;
use App\Invoice;
use App\User;
use App\Stock;

class StockSaleFeatureTest extends TestCase
{
    use DatabaseMigrations;

    // TODO:do assertions fo actual views - later?

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }


    /** @test */
    public function can_view_stock_and_sales()
    {
        $this->disableExceptionHandling();
        factory(Stock_Sale::class, 11)->create();
        $this->assertEquals(11, Stock_Sale::count());

        $response = $this->actingAs($this->user)->get('/stocksales');
        $stockSales = Stock_Sale::all();

        $response->assertStatus(200);
        $response->assertViewIs('stock_sale.index');
        $this->assertTrue($response->data('stockSales')->diff($stockSales)->count()==0);
    }

    /** @test */
    public function stock_sales_entries_can_be_created()
    {
        $this->disableExceptionHandling();
        $this->assertEquals(0, Stock_Sale::count());

        $response = $this->actingAs($this->user)->post('/stocksales/create', [
            'ataco_code'=>factory(Product::class)->create()->ataco_code,
            'kat_num'=>4990,
            'opis'=>'Opis',
            'prev_total_stock'=>50,
            'shipment'=>0,
            'sbrijeg'=>100,
            'bihac'=>100,
            'tuzla'=>100,
            'sarajevo'=>100,
            'laktasi'=>100,
            'total_stock'=>500,
            'date'=>Carbon::parse('2018-01-01'),
            'type'=>'Stock'
            ]);

        $this->assertEquals(1, Stock_Sale::count());

        // dd(Carbon::parse('11.10.2018')->month);
    }

    /** @test */
    public function stock_sale_view_is_available()
    {
        $this->disableExceptionHandling();

        factory(Stock_Sale::class, 11)->create();

        $response = $this->actingAs($this->user)->get('/stocksales/all');

        $response->assertStatus(200);
        $response->assertViewIs('stock_sale.index_all');
        $response->assertSeeText("Stock Sales");
    }

    /** @test */
    public function stock_sale_create_view_is_available()
    {
        $this->disableExceptionHandling();

        $response = $this->actingAs($this->user)->get('/stocksales/create');

        $response->assertStatus(200);
        $response->assertViewIs('stock_sale.create');
        $response->assertSee("Insert Ataco stock_sales");
    }

    /** @test */
    public function stock_sales_can_be_created_from_text()
    {
        $this->disableExceptionHandling();

        $product1 = factory(Product::class)->create(['ataco_code'=>'10001051']);
        $product2 = factory(Product::class)->create(['ataco_code'=>'10001061']);
        $product3 = factory(Product::class)->create(['ataco_code'=>'10001070']);
        $product3 = factory(Product::class)->create(['ataco_code'=>'10001071']);

        $stock_sales = "Redni	kat_num	ataco_code	Opis	prev_total_stock	Shipment	S.Brijeg	Bihac	Tuzla	Sarajevo	Laktasi	Total_stock	Date	Type
10	41000 	10001051	HIPP 1 ORGANIC 300G	943	0	203	54	60	106	128	551	2017-06-11	Stock
12	41000 	10001061	HIPP 1 ORGANIC 800G	914	0	265	75	48	139	221	748	2017-06-11	Stock
14	41000 	10001071	HIPP 1 COMBIOTIC INFANT 300G	2,050	0	458	55	118	165	422	1,218	2017-06-11	Stock";
        $csv = Stock_Sale::getStockSalesFromText($stock_sales);


        $this->assertEquals(3, $csv->count());

        // dd($csv->toarray());

        $response = $this->actingAs($this->user)->post("/stocksales/create", $csv->toarray());

        // Stock_Sale::saveEntries($csv);
        $this->assertEquals(3, Stock_Sale::count());
    }

    //Disabled for case when relationship betwwn stock_sale and products is not available
    // /** @test */
    // public function sale_creates_entry_in_the_current_stock()
    // {
    //     $this->disableExceptionHandling();

    //     $this->assertEquals(0, Stock::count());


    //     $product1 = factory(Product::class)->create(['ataco_code'=>'10001']);
    //     $product2 = factory(Product::class)->create(['ataco_code'=>'10002']);
    //     $product3 = factory(Product::class)->create(['ataco_code'=>'10003']);

    //     $stock_sales ="Redni	kat_num	ataco_code	Opis	prev_total_stock	Shipment	S.Brijeg	Bihac	Tuzla	Sarajevo	Laktasi	Total_stock	Date	Type
    // 10	41000 	10001	HIPP 1 ORGANIC 300G	943	0	203	54	60	106	128	551	2017-06-11	Stock
    // 12	41000 	10002	HIPP 1 ORGANIC 800G	914	0	100	50	50	200	50	450	2017-06-11	Sale
    // 14	41000 	10003	HIPP 1 COMBIOTIC INFANT 300G	2,050	0	458	55	118	165	422	1,218	2017-06-11	Stock";

    //     $csv = Stock_Sale::getStockSalesFromText($stock_sales);
    //     $this->assertEquals(3, $csv->count());

    //     $response = $this->actingAs($this->user)->post("/stocksales/create", $csv->toarray());
    //     $this->assertEquals(3, Stock_Sale::count());
    //     $this->assertEquals(1, Stock::count());

    //     $this->assertEquals(-450, Stock::first()->amount);
    // }

    /** @test */
    public function stock_sale_json_is_available()
    {
        // $this->disableExceptionHandling();

        $data = [
            'ataco_code'=>11001,
            'kat_num'=>4990,
            'opis'=>'Opis',
            'prev_total_stock'=>50,
            'shipment'=>0,
            'sbrijeg'=>100,
            'bihac'=>100,
            'tuzla'=>100,
            'sarajevo'=>100,
            'laktasi'=>100,
            'total_stock'=>500,
            'date'=>Carbon::parse('2018-01-01'),
            'type'=>'Stock',
            'month'=>1,
            'year'=>2018,
        ];

        $product1 = factory(Product::class)->create(['ataco_code'=>11001]);

        factory(Stock_Sale::class, 10)->create();
        factory(Stock_Sale::class)->create($data);
        $this->assertEquals(11, Stock_Sale::count());

        $response = $this->actingAs($this->user)->json('GET', '/stocksales/json');



        $response->assertSuccessful();
        $response->assertStatus(200);
        $data = Stock_Sale::all()->toArray();
        // dd($data);
        $response->assertJson($data);
    }


    /** @test */
    public function user_can_search_threads()
    {
        $this->withoutExceptionHandling();

        config(['scout.driver' => 'algolia']);

        $search = '4440001';

        $product1 = factory(Product::class)->create(['code'=>$search, 'ataco_code'=>$search]);

        factory(Stock_Sale::class, 2)->create();
        factory(Stock_Sale::class, 2)->create(['ataco_code'=>$search]);

        // dd(Stock_Sale::all()->toArray());
        do {
            sleep(.25);
            $results = $this->actingAs($this->user)->getJson("/stocksales/search?q={$search}")->json()['data'];
        } while (empty($results));
        // dd($results);

        $this->assertCount(2, $results);

        Stock_Sale::latest()->take(4)->unsearchable();
    }
}
