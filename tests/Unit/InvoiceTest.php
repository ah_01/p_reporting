<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Invoice;
use Carbon\Carbon;
use App\Product;
use Illuminate\Support\Facades\DB;
use App\User;

class InvoiceTest extends TestCase
{
    use DatabaseMigrations;

    private function validParams($overrides=[])
    {
        return array_merge([
            'delivery_date'=>'2018-01-11',
            'product_id' => 1,
            'code'=>1001,
            'pcs'=>0,
            'price_unit'=>1,
            'total_price'=>0,
            'product_total'=>0,
            'expiry_date'=>'2018-01-07',
            'expiry_date'=>Carbon::parse('2018-01-01')->addYear(),
            'type'=>"stock",
        ], $overrides);
    }

    /**
     * @test
     *
     * @return void
     */
    public function invoice_can_be_created()
    {
        $invoice = factory(Invoice::class)->create();

        $this->assertEquals(Invoice::count(), 1);
    }

    /** @test */
    public function multiple_invoices_can_be_created()
    {
        $invoice = factory(Invoice::class, 10)->create();

        $this->assertEquals(Invoice::count(), 10);
    }

    /** @test */
    public function invoice_number_must_be_unique()
    {
        $invoice1 = factory(Invoice::class)->create(['invoice_number'=>111101]);

        try {
            $invoice2 = factory(Invoice::class)->create(['invoice_number'=>111101]);
        } catch (\Exception $e) {
            $this->assertEquals(Invoice::count(), 1);
            return;
        }

        $this->fail('Invoice number must be unique integer value');
    }

    /** @test */
    public function invoice_has_propper_delivery_date_field()
    {
        factory(Invoice::class, 10)->create();
        factory(Invoice::class)->create(['delivery_date'=>'2018-05-01']);

        $invoices = Invoice::all();


        $this->assertEquals(Invoice::count(), 11);
        $this->assertEquals($invoices->last()->delivery_date, Carbon::parse('2018-05-01'));
    }

    /** @test */
    public function invoice_can_have_one_or_more_products()
    {
        $invoice = factory(Invoice::class)->create();
        $products = factory(Product::class, 10)->create();

        $products->each(function ($product) use ($invoice) {
            $invoice->products()->attach($product, $this->validParams(['product_id'=>$product->id]));
        });
        // $invoice->products()->saveMany($products);

        $product = factory(Product::class)->create();

        $invoice->storeProduct($product, $this->validParams(['product_id'=>$product->id]));
        $invoice->storeProduct($product, $this->validParams(['product_id'=>$product->id]));


        $this->assertEquals($invoice->fresh()->products()->count(), 12);

        $invoice->products()->detach($products->last()->id);

        $this->assertEquals($invoice->fresh()->products()->count(), 11);

        // $invoice->products()->detach($product->id); //remved 2 products this could be bug or to be exact it is in this setup
        // $this->assertEquals($invoice->fresh()->products()->count(), 9);




        ($invoice->products()->newPivotStatement()->whereInvoiceId($invoice->id)->whereProductId($product->id)->delete());
        $this->assertEquals($invoice->fresh()->products()->count(), 9);
    }

    /** @test */
    public function invoice_product_must_have_additional_fields()
    {
        $invoice = factory(Invoice::class)->create();
        $product = factory(Product::class)->create();

        // $invoice_product = [
        //     'truck_date'=>$invoice->delivery_date,
        //     'product_id' => $product->id,
        //     'code'=>$product->code,
        //     'pcs'=>200,
        //     'price_unit'=>3,
        //     'total_price'=>600,
        //     'expiry_date'=>'2018-01-07',
        //     'expiry_date'=>Carbon::parse('2018-01-01')->addYear(),
        //     'type'=>"stock",
        // ];

        $invoice_product = $this->validParams(['delivery_date'=>$invoice->delivery_date, 'product_id' => $product->id,  'code'=>$product->code,]);

        $this->assertEquals($invoice->products()->count(), 0);
        $invoice->storeProduct($product, $invoice_product);

        $this->assertEquals($invoice->products()->count(), 1);

        $p = $invoice->products()->first();

        $this->assertEquals($p->pivot->code, $product->code);
        $this->assertEquals($p->pivot->price_unit, 1);
        $this->assertEquals($p->pivot->pcs, 0);
        $this->assertEquals($p->pivot->total_price, $invoice_product['pcs'] * $invoice_product['price_unit']);
    }

    /** @test */
    public function total_price_field_should_be_calculated_if_empty()
    {
        $invoice = factory(Invoice::class)->create();
        $product = factory(Product::class)->create();


        $invoice_product = $this->validParams(['pcs'=>200, 'price_unit'=>3, 'total_price'=>600]);


        $this->assertEquals($invoice->products()->count(), 0);
        $invoice->storeProduct($product, $invoice_product);
        $this->assertEquals($invoice->products()->count(), 1);
        $this->assertEquals($invoice->products()->first()->pivot->total_price, 600);
    }


    /** @test */
    public function invoice_have_grand_total_method()
    {
        // productTotalPcs(Product::class) will calculate total of products arrived in every invoice
        $invoice1 = factory(Invoice::class)->create(['delivery_date'=>'2018-01-05']);
        $invoice2 = factory(Invoice::class)->create(['delivery_date'=>'2018-01-15']);

        $productA = factory(Product::class)->create(['code'=>1101]);
        $productB = factory(Product::class)->create(['code'=>1103]);


        $this->assertEquals(Invoice::count(), 2);
        $this->assertEquals(Product::count(), 2);

        $invoice1->products()->attach($productA, $this->validParams([
            'product_id' => $productA->id,
            'code'=>$productA->code,
            'pcs'=>100
        ]));

        $invoice1->products()->attach($productA, $this->validParams([
            'product_id' => $productA->id,
            'code'=>$productA->code,
            'pcs'=>500
        ]));

        $invoice1->products()->attach($productB, $this->validParams([
            'product_id' => $productB->id,
            'code'=>$productB->code,
            'pcs'=>500
        ]));

        $this->assertEquals($invoice1->fresh()->products()->count(), 3);


        $invoice1->products()->attach($productA, $this->validParams([
            'product_id' => $productA->id,
            'code'=>$productA->code,
            'pcs'=>100

        ]));

        $invoice2->products()->attach($productA, $this->validParams([
            'product_id' => $productA->id,
            'code'=>$productA->code,
            'pcs'=>100

        ]));

        $this->assertEquals($invoice2->fresh()->products()->count(), 1);

        $this->assertEquals(700, $invoice1->productTotalPcs($productA));
        $this->assertEquals(500, $invoice1->productTotalPcs($productB));
        $this->assertEquals(100, $invoice2->productTotalPcs($productA));
    }

    /** @test */
    public function invoice_related_products_have_running_total_count()
    {
        // Running total shpuld calculate all invoices related to specific product
        // create 2 products
        // Create invoice
        // add products with various pcs to invoice
        // get grand total
        // add new invoice
        // add products with various pcs to invoice
        // get grand total wit new values added

        // do tests
        $this->disableExceptionHandling();


        $product1 = factory(Product::class)->create(['code'=>1101]);
        $product2 = factory(Product::class)->create(['code'=>1502]);

        $invoice = factory(Invoice::class)->create(['delivery_date'=>'2018-01-05']);
        $invoice2 = factory(Invoice::class)->create(['delivery_date'=>'2018-01-11']);

        $this->assertEquals(0, $invoice->products()->count());


        $invoice->storeProduct($product1, $this->validParams(['code'=>$product1->code, 'product_id'=>$product1->id, 'pcs'=>50, 'delivery_date'=>'2018-01-7']));
        $this->assertEquals(50, $invoice->fresh()->products->first()->pivot->product_total);
        $invoice->storeProduct($product1, $this->validParams(['code'=>$product1->code, 'product_id'=>$product1->id, 'pcs'=>200, 'delivery_date'=>'2018-01-7']));
        $this->assertEquals(250, $invoice->fresh()->products->last()->pivot->product_total);

        $invoice2->storeProduct($product1, $this->validParams(['code'=>$product1->code, 'product_id'=>$product1->id, 'pcs'=>400, 'delivery_date'=>'2018-01-7']));
        $invoice2->storeProduct($product2, $this->validParams(['code'=>$product2->code, 'product_id'=>$product2->id, 'pcs'=>700, 'delivery_date'=>'2018-01-7']));

        $invoice->storeProduct($product1, $this->validParams(['code'=>$product1->code, 'product_id'=>$product1->id, 'pcs'=>200, 'delivery_date'=>'2018-01-7']));
        // dd($invoice->fresh()->products->last()->pivot->product_total);
        $this->assertEquals(850, $invoice->fresh()->products->last()->pivot->product_total);

        $this->assertEquals(3, $invoice->products()->count());
        $this->assertEquals(2, $invoice2->products()->count());
        // $invoice->storeProduct($product1, $this->validParams(['code'=>$product1->code, 'product_id'=>$product1->id, 'pcs'=>100, 'delivery_date'=>'2018-01-7']));


        // dump($invoice->getProductGroup($product1)->last()->pivot->product_total);
        $this->assertEquals(850, $invoice2->fresh()->runningTotalPcs($product1));
        $this->assertEquals(700, $invoice->fresh()->runningTotalPcs($product2));
        // $this->assertEquals(400, $invoice->fresh()->runningTotalPcs($product2));

        // $invoice2->storeProduct($product1, $this->validParams(['code'=>$product1->code, 'product_id'=>$product1->id, 'pcs'=>50, 'delivery_date'=>'2018-01-7']));


        // $this->assertEquals(300, $invoice->fresh()->runningTotalPcs($product1));

        // $invoice->storeProduct($product2, $this->validParams(['code'=>$product2->code, 'product_id'=>$product2->id, 'pcs'=>400, 'delivery_date'=>'2018-01-7']));
        // $invoice->storeProduct($product2, $this->validParams(['code'=>$product2->code, 'product_id'=>$product2->id, 'pcs'=>400, 'delivery_date'=>'2018-01-7']));

        // $this->assertEquals(6, $invoice->products()->count());
        // // dump($invoice->getProductGroup($product1)->last()->pivot->product_total);
        // $this->assertEquals(350, $invoice->fresh()->runningTotalPcs($product1));
        // $this->assertEquals(1200, $invoice->fresh()->runningTotalPcs($product2));
        // $this->assertEquals(1200, $invoice->fresh()->productTotalPcs($product2));
    }

    /** @test */
    public function get_product_info_and_check_if_all_products_from_text_exists()
    {
        $text = "code	title	pcs	price_unit	total_price	expiry_date
AL00002-F-U	*HiPP 1 Combiotic Infant Milk (p)	1.120,00	3,580	4.009,60	03.02.2019
AL2013-U	*HiPP 1 Combiotic Infant Milk (p)	1.024,00	8,110	8.304,64	30.12.2018
AL2016-02-U	*HiPP 1 Organic Infant Milk (p)	1.680,00	3,150	5.292,00	06.07.2019";

        //if some of products are not present change the code approprately or stop proces?

        factory(Product::class)->create(['code'=>'AL00002-F-U']);
        factory(Product::class)->create(['code'=>'AL2016-02-U']);
        $this->assertEquals(2, Product::count());

        // dd(Product::where('code', 'AL2013-U')->exists());
        $this->expectException(\Exception::class);
        $csv = Invoice::getProductInfoFromText($text);
        $this->assertEquals(3, $csv->count());

        // // dd($csv->groupBy('code')->keys());

        // $this->assertContains('NONE-AL2013-U', $csv->groupBy('code')->keys());
        // $this->assertContains('AL00002-F-U', $csv->groupBy('code')->keys());
        // $this->assertContains('AL2016-02-U', $csv->groupBy('code')->keys());
    }

    /** @test */
    public function guest_cant_see_invoice_page()
    {
        $invoice = factory(Invoice::class)->create();
        $this->assertEquals(1, Invoice::count());

        $response = $this->get('/invoice');
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }
    /** @test */
    public function user_can_see_invoice_page()
    {
        $invoice = factory(Invoice::class)->create();
        $User = factory(User::class)->create();

        $response = $this->actingAs($User)->get('/invoice');
        $response->assertStatus(200);
    }


    /** @test */
    public function add_products_to_invoice_from_text()
    {
        // FIXME: this test is not feature test it is unit test  look further
        // FIXME: need to add test where sending text to endpoint will add products to invoice

        $this->disableExceptionHandling();

        $invoice = factory(Invoice::class)->create([
                'invoice_number'=>10001,
                'delivery_date'=>'2018-01-01',
            ]);
        $product1 = factory(Product::class)->create(['code'=>'AL2013-U']);
        $product2 = factory(Product::class)->create(['code'=>'AL2012-F-U']);
        $product3 = factory(Product::class)->create(['code'=>'AL2016-02-U']);

        $this->assertEquals(1, Invoice::count());
        $this->assertEquals(10001, Invoice::first()->invoice_number);

        $text = "code	title	pcs	price_unit	total_price	expiry_date
AL2012-F-U	*HiPP 1 Combiotic Infant Milk (p)	1.120,00	3,580	4.009,60	03.02.2019
AL2013-U	*HiPP 1 Combiotic Infant Milk (p)	1.024,00	8,110	8.304,64	30.12.2018
AL2016-02-U	*HiPP 1 Organic Infant Milk (p)	1.680,00	3,150	5.292,00	06.07.2019";
        //TODO: column name total needs to be total_price to follow DB column names
        //TODO: column name expiry_date needs to be expiry_date to follow DB column names



        $csv = Invoice::getProductInfoFromText($text);

        $this->assertEquals(3, $csv->count());

        $invoice->storeProductsFromCsv($csv);

        $this->assertEquals(3, $invoice->fresh()->products->count());
        // dd($invoice->products->toArray());
    }
    //TODO: Products on invoice should be added as stock value to current stock table
}
