<?php

namespace Tests\Unit;

use Carbon\Carbon;
use App\Stock_Sale;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Product;
use App\User;

class StockSaleTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function stock_sale_can_be_created()
    {
        $product = factory(Product::class)->create(['ataco_code'=>10000]);
        factory(Stock_Sale::class)->create(['ataco_code'=>10000]);

        $this->assertEquals(1, Stock_Sale::count());
    }

    /** @test */
    public function stock_and_sales_have_all_fields()
    {
        $this->disableExceptionHandling();
        $product = factory(Product::class)->create(['ataco_code'=>10000,'code'=>'hipp0001', 'id'=>200]);
        factory(Stock_Sale::class)->create([
            'ataco_code'=>10000,
            'kat_num'=>4900,
            'opis'=>'Opis',
            'prev_total_stock'=>100,
            'shipment'=>0,
            'sbrijeg'=>100,
            'bihac'=>100,
            'tuzla'=>100,
            'sarajevo'=>100,
            'laktasi'=>100,
            'total_stock'=>100,
            'date'=>'2018-03-01',
            'type'=>'Stock',
        ]);

        $this->assertEquals(1, Stock_Sale::count());
        $stockSales = Stock_Sale::first();

        $this->assertEquals(10000, $stockSales->ataco_code);
        $this->assertEquals(4900, $stockSales->kat_num);
        $this->assertEquals('Opis', $stockSales->opis);
        $this->assertEquals(100, $stockSales->prev_total_stock);
        $this->assertEquals(0, $stockSales->shipment);
        $this->assertEquals(100, $stockSales->sbrijeg);
        $this->assertEquals(100, $stockSales->bihac);
        $this->assertEquals(100, $stockSales->tuzla);
        $this->assertEquals(100, $stockSales->sarajevo);
        $this->assertEquals(100, $stockSales->laktasi);
        $this->assertEquals(100, $stockSales->total_stock);
        $this->assertEquals(Carbon::parse('2018-03-01'), $stockSales->date);


        // Automatic creation fileds
        $this->assertEquals('hipp0001', $stockSales->code);
        // TODO:fix if realtion to product for stocksales is resolved
        // $this->assertEquals(200, $stockSales->product_id);
        $this->assertEquals('10000_2018-03-01_Stock', $stockSales->acode_date_type);
        $this->assertEquals(2018, $stockSales->year);
        $this->assertEquals(3, $stockSales->month);
        $this->assertEquals(0, $stockSales->error);
        $this->assertEquals("0", $stockSales->average);
    }
    /** @test */
    public function sale_type_has_correct_average()
    {
        $this->withExceptionHandling();
        $product = factory(Product::class)->create(['ataco_code'=>10000,'code'=>'hipp0001', 'id'=>200]);
        factory(Stock_Sale::class)->create([
            'ataco_code'=>10000,
            'kat_num'=>4900,
            'opis'=>'Opis',
            'prev_total_stock'=>100,
            'shipment'=>0,
            'sbrijeg'=>100,
            'bihac'=>100,
            'tuzla'=>100,
            'sarajevo'=>100,
            'laktasi'=>19,
            'total_stock'=>100,
            'date'=>'2018-03-01',
            'type'=>'Sale',
        ]);

        $this->assertEquals(1, Stock_Sale::count());
        $stockSales = Stock_Sale::first();
        $this->assertEquals(84, $stockSales->average);
    }

    /** @test */
    public function stock_sale_has_correct_oos_error_field()
    {
        // of current stock is less then last sale * 2 OOS error
        $this->disableExceptionHandling();
        $product = factory(Product::class)->create(['ataco_code'=>10000,'code'=>'hipp0001', 'id'=>200]);
        factory(Stock_Sale::class)->create([
            'ataco_code'=>10000,
            'kat_num'=>4900,
            'opis'=>'Opis',
            'prev_total_stock'=>100,
            'shipment'=>0,
            'sbrijeg'=>100,
            'bihac'=>100,
            'tuzla'=>100,
            'sarajevo'=>100,
            'laktasi'=>100,
            'total_stock'=>100,
            'date'=>'2018-03-01',
            'type'=>'Stock',
        ]);

        factory(Stock_Sale::class)->create([
            'ataco_code'=>10000,
            'kat_num'=>4900,
            'opis'=>'Opis',
            'prev_total_stock'=>100,
            'shipment'=>0,
            'sbrijeg'=>90,
            'bihac'=>90,
            'tuzla'=>90,
            'sarajevo'=>90,
            'laktasi'=>90,
            'total_stock'=>100,
            'date'=>'2018-03-01',
            'type'=>'Sale',
        ]);

        $stockSales = Stock_Sale::all();
        // dd($stockSales);
        $this->assertEquals(2, $stockSales->count());

        $stock = $stockSales->shift();
        $sale = $stockSales->shift();

        $this->assertEquals(0, $stock->average);
        $this->assertEquals(90, $sale->average);
        $this->assertEquals("oos", $sale->error);
    }

    /** @test */
    public function stock_sale_has_correct_error_for_overstock()
    {
        $this->disableExceptionHandling();
        $product = factory(Product::class)->create(['ataco_code'=>10000,'code'=>'hipp0001', 'id'=>200]);
        factory(Stock_Sale::class)->create([
            'ataco_code'=>10000,
            'kat_num'=>4900,
            'opis'=>'Opis',
            'prev_total_stock'=>100,
            'shipment'=>0,
            'sbrijeg'=>800,
            'bihac'=>50,
            'tuzla'=>50,
            'sarajevo'=>50,
            'laktasi'=>50,
            'total_stock'=>1000,
            'date'=>'2018-03-01',
            'type'=>'Stock',
        ]);
        factory(Stock_Sale::class)->create([
            // 'ataco_code'=>10000,
            'kat_num'=>4900,
            'opis'=>'Opis',
            'prev_total_stock'=>100,
            'shipment'=>0,
            'sbrijeg'=>50,
            'bihac'=>50,
            'tuzla'=>50,
            'sarajevo'=>700,
            'laktasi'=>50,
            'total_stock'=>1000,
            'date'=>'2018-03-01',
            'type'=>'Stock',
        ]);

        // dd($stockSales);
        $this->assertEquals(2, Stock_Sale::count());

        $this->assertContains("sbrijeg-over", Stock_Sale::first()->error);
        $this->assertContains("sbrijeg", Stock_Sale::first()->error);
        // dd(Stock_Sale::latest('id')->first()->toArray());
        $this->assertContains("sarajevo-over", Stock_Sale::latest('id')->first()->error);
    }

    /** @test */
    public function stock_sale_has_correct_totals_error_field()
    {
        // if PREV_TOTAL_STOCK != TOTAL_STOCK(SALE) + TOTAL_STOCK(STOCK) for DATE -> error TOTALS
        $this->disableExceptionHandling();
        $product = factory(Product::class)->create(['ataco_code'=>10000,'code'=>'hipp0001', 'id'=>200]);
        factory(Stock_Sale::class)->create([
            'ataco_code'=>10000,
            'kat_num'=>4900,
            'opis'=>'Opis',
            'prev_total_stock'=>500,
            'shipment'=>0,
            'sbrijeg'=>100,
            'bihac'=>100,
            'tuzla'=>100,
            'sarajevo'=>100,
            'laktasi'=>100,
            'total_stock'=>350,
            'date'=>'2018-03-01',
            'type'=>'Stock',
        ]);

        factory(Stock_Sale::class)->create([
            'ataco_code'=>10000,
            'kat_num'=>4900,
            'opis'=>'Opis',
            'prev_total_stock'=>null,
            'shipment'=>null,
            'sbrijeg'=>30,
            'bihac'=>30,
            'tuzla'=>30,
            'sarajevo'=>30,
            'laktasi'=>30,
            'total_stock'=>150,
            'date'=>'2018-03-01',
            'type'=>'Sale',
        ]);
        $stockSales = Stock_Sale::all();
        $this->assertEquals(2, $stockSales->count());

        $stock = $stockSales->shift();
        $sale = $stockSales->pop();

        $this->assertEquals(0, $stock->error);
        $this->assertEquals(0, $sale->error);

        Stock_Sale::checkForErrors();

        $stockSales = Stock_Sale::all();
        $this->assertEquals(2, $stockSales->count());

        $stock = $stockSales->shift();
        $sale = $stockSales->pop();

        // This test is pointless to many TOTALS errors
        // $this->assertContains('TOTALS', $stock->error);
        // $this->assertContains('TOTALS', $sale->error);
    }

    /** @test */
    public function stock_and_sales_are_unique_on_code_date_and_type()
    {
        $product = factory(Product::class)->create(['ataco_code'=>1101,]);
        factory(Stock_Sale::class)->create(['ataco_code'=>1101, 'date'=>'2018-01-01', 'type'=>'stock']);
        factory(Stock_Sale::class)->create(['ataco_code'=>1101, 'date'=>'2018-01-01', 'type'=>'sale']);


        $this->assertEquals(2, Stock_Sale::count());


        $this->expectException(\Exception::class);
        factory(Stock_Sale::class)->create(['ataco_code'=>1101, 'date'=>'2018-01-01', 'type'=>'sale']);
    }

    /** @test */
    public function every_record_has_related_product()
    {
        $this->withoutExceptionHandling();
        $product = factory(Product::class)->create(['ataco_code'=>10001, 'code'=>3001]);
        $stock_sale = factory(Stock_Sale::class)->create(['ataco_code'=>10001, 'product_id'=>$product->id]);

        $this->assertEquals(1, Stock_Sale::count());
        $this->assertEquals(1, Product::count());


        $this->assertEquals(3001, $stock_sale->product->code);
    }

    /** @test */
    public function stock_sale_retrieves_and_stores_product_code_during_creating()
    {
        $product = factory(Product::class)->create(['ataco_code'=>10001, 'code'=>'3001-01']);
        $stock_sale = factory(Stock_Sale::class)->create(['ataco_code'=>10001]);

        $this->assertEquals(1, Stock_Sale::count());
        $this->assertEquals(1, Product::count());
        //Added test for getProductCode method
        $this->assertEquals(3001, $stock_sale->getProduct($product->ataco_code)->code);
        $this->assertEquals(01, $stock_sale->getProduct($product->ataco_code)->code_version);
        $this->assertEquals(10001, $stock_sale->getProduct($product->ataco_code)->ataco_code);
        // $this->assertEquals(3001, $stock_sale->product->code);
        // $this->expectsEvents(\App\Events\StockSalesSaved::class);
        $this->assertEquals(3001, $stock_sale->code);
    }

    /** @test */
    public function recalculate_products_codes_and_types()
    {
        $stock_sale = factory(Stock_Sale::class, 3)->create(['ataco_code'=>10001, 'code'=>'AL2016-01']);
        $product = factory(Product::class)->create(['ataco_code'=>10001, 'code'=>'AL2016-01', 'category'=>'milk']);

        $this->assertEquals(3, Stock_Sale::count());
        $this->assertEquals(1, Product::count());

        $stockSales = Stock_Sale::all();
        $stockSales->each(function ($item) {
            $this->assertEquals('NO CODE', $item->code);
            $this->assertEquals(null, $item->category);
        });

        Stock_Sale::reCheck();
        $stockSales = Stock_Sale::all();

        $stockSales->each(function ($item) {
            $this->assertEquals('AL2016', $item->code);
            $this->assertEquals('milk', $item->product_category);
        });
    }


    /** @test */
    public function stock_sales_filtering_out_null_records()
    {
        // In current reports there is to many null records  - about 50% we need to remove noise

        $this->withoutExceptionHandling();

        $product1 = factory(Product::class)->create(['ataco_code'=>'10001051']);
        $product2 = factory(Product::class)->create(['ataco_code'=>'10001061']);
        $product3 = factory(Product::class)->create(['ataco_code'=>'10001070']);
        $product3 = factory(Product::class)->create(['ataco_code'=>'10001071']);

        $user = factory(User::class)->create();

        $stock_sales =
"Redni	kat_num	ataco_code	Opis	prev_total_stock	Shipment	S.Brijeg	Bihac	Tuzla	Sarajevo	Laktasi	Total_stock	Date	Type
1	4150	10001001	4150 HIPP 1 mlijeko 300 G	0	0	0	0	50	50	0	100	2018-01-01	Stock
2	4155	10001010	4155 HIPP 2 mlijeko 300 G	0	0	0	0	0	0	0	0	2018-01-01	Stock
3	4170	10001020	4170 HIPP 3 mlijeko vanilija 450 G	0	0	0	0	0	0	0	0	2018-01-01	Stock
4	4171	10001021	4171 HIPP 3 mlijeko voće 450 G	0	0	0	0	0	0	0	0	2018-01-01	Stock
5	4145	10001030	4145 PRE HIPP mlijeko 300 G	0	0	0	0	0	0	0	0	2018-01-01	Stock
6	4180	10001040	4180 HIPP MLIJEKO H. A. 1 300 G	0	0	0	0	0	0	0	0	2018-01-01	Stock
7	4181	10001045	4181 HIPP MLIJEKO H. A. 2 300 G	100	0	0	0	0	0	0	0	2018-01-01	Stock
8	4045	10001046	4045 HR 2043 HIPP 1PROBIOTIK 300 G 	100	0	0	0	0	0	0	0	2018-01-01	Stock";
        $csv = Stock_Sale::getStockSalesFromText($stock_sales);
        // dd($csv->toArray());
        $this->assertEquals(8, $csv->count());


        $sale=
        "Redni	kat_num	ataco_code	Opis	prev_total_stock	Shipment	S.Brijeg	Bihac	Tuzla	Sarajevo	Laktasi	Total_stock	Date	Type
9	4181	10001045	4181 HIPP MLIJEKO H. A. 2 300 G			0	0	0	0	0	0	2018-01-01	Sale
10	4045	10001046	4045 HR 2043 HIPP 1PROBIOTIK 300 G 			0	0	0	0	0	0	2018-01-01	Sale
11	4050	10001050	HIPP 1 BIO mlijeko 300g			0	0	0	0	0	0	2018-01-01	Sale
12	4051	10001051	HIPP 1 ORGANIC 300G			129	14	9	30	280	462	2018-01-01	Sale
13	4060	10001060	HIPP 1 BIO mlijeko 800g			0	0	0	0	0	0	2018-01-01	Sale";
        $sales = Stock_Sale::getStockSalesFromText($sale);


        $response = $this->actingAs($user)->post("/stocksales/create", $csv->toarray());
        $response = $this->actingAs($user)->post("/stocksales/create", $sales->toarray());

        $this->assertEquals(2, Stock_Sale::count());
    }
}
