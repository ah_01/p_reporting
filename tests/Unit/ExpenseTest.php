<?php

namespace Tests\Unit;

use App\Expense;
use Tests\TestCase;
use Mockery\Exception;
use Illuminate\Support\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ExpenseTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function expense_can_be_created()
    {
        $this->disableExceptionHandling();

        $expense = factory(Expense::class)->create();

        $this->assertEquals(1, Expense::count());
    }

    /** @test */
    public function expense_model_has_required_fieleds()
    {
        $expense = factory(Expense::class)->create(['amount'=>200, 'title'=>'Naslov', 'ataco_code'=>10001, 'kat_num'=>4900]);

        $this->assertEquals(1, Expense::count());

        $this->assertEquals(200, $expense->amount);
        $this->assertEquals('Naslov', $expense->title);
        $this->assertEquals(10001, $expense->ataco_code);
        $this->assertEquals(4900, $expense->kat_num);

        $this->assertContains($expense->type, ['kalo', 'rashod', 'donacija', 'akcija']);
    }

    /** @test */
    public function get_formatted_date_attribute()
    {
        $expense = factory(Expense::class)->create();

        $this->assertEquals(Carbon::parse('2018-01-01'), $expense->expense_date, 'No valid date format!');
    }

    /** @test */ //FIXME:this test needs to be fixed - Fixed already?!!!
    public function expense_must_be_unique_based_on_code_date_and_type()
    {
        $expense = factory(Expense::class)->create(['ataco_code'=>10001,'expense_date'=>'2018-01-01', 'type'=>'Stock']);
        $expense = factory(Expense::class)->create(['ataco_code'=>10001,'expense_date'=>'2018-01-01', 'type'=>'Sale']);

        $this->assertEquals(2, Expense::count());

        $this->expectException(\Exception::class);
        $expense = factory(Expense::class)->create(['ataco_code'=>10001,'expense_date'=>'2018-01-01', 'type'=>'Sale']);
    }
}
