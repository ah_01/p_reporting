<?php
namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Truck;
use Illuminate\Support\Carbon;

class TruckTest extends TestCase
{
    use DatabaseMigrations;
    /** @test */
    public function truck_can_be_created()
    {
        $truck = factory(Truck::class)->create(['number'=>22]);

        $this->assertEquals(22, $truck->number, 'Error no valid turck number');
        $this->assertEquals(1, Truck::count(), 'Number of models  not correct');
    }


    /** @test */
    public function truck_number_is_unique()
    {
        factory(Truck::class)->create(['number'=>22, 'year'=>2017]);
        $this->assertEquals(Truck::count(), 1);

        $this->expectException(\Exception::class);
        factory(Truck::class)->create(['number'=>22, 'year'=>2017]);
    }

    /** @test */
    public function get_formatted_date_attribute()
    {
        $truck = factory(Truck::class)->create(['delivery_date'=>'2018-01-01']);
        // dd($truck->delivery_date);

        $this->assertEquals(Carbon::parse('2018-01-01'), $truck->delivery_date, 'No valid date format!');
    }

    /** @test */
    public function truck_has_proper_number_year_field()
    {
        $truck = factory(Truck::class)->create([
                    'number'=>19,
                    'year'=>2017,
                    ]);

        $this->assertEquals($truck->year, 2017);
        $this->assertEquals($truck->number_year, '19_2017');
    }
}
