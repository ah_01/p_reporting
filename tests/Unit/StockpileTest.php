<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Stockpile;
use Carbon\Carbon;
use App\Product;
use App\Stock_Sale;

class StockpileTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function create_one_stockpile_item()
    {
        $this->disableExceptionHandling();

        factory(Stockpile::class)->create();

        $this->assertEquals(1, Stockpile::count());
    }

    /** @test */
    public function stockpile_has_formatted_truck_date_field()
    {
        $this->disableExceptionHandling();
        factory(Stockpile::class)->create([
                'date'=>'01.01.2018',
            ]);

        // dd(Carbon::parse('01.01.2018')->month);

        $this->assertEquals(Carbon::parse('01.01.2018')->toDateString(), Carbon::parse(Stockpile::first()->date)->toDateString());
    }

    /** @test */
    public function stockpile_item_has_formatted_exp_date_field()
    {
        $this->disableExceptionHandling();
        factory(Stockpile::class)->create([
                'exp_date'=>'2018-01-01',
            ]);

        $this->assertEquals(Carbon::parse('2018-01-01')->toDateString(), Carbon::parse(Stockpile::first()->exp_date)->toDateString());
    }


    /** @test */
    public function stockpile_has_precalculated_month_and_year_fields()
    {
        $this->disableExceptionHandling();

        factory(Product::class)->create(['ataco_code'=>10001, 'code'=>'HR10001']);
        factory(Stockpile::class)->create([
            'ataco_code'=>10001,
            'date'=>'2018-05-01',
        ]);

        $this->assertEquals(2018, Stockpile::first()->year);
        $this->assertEquals(5, Stockpile::first()->month);
    }

    /** @test */
    public function stockpile_item_has_formatted_price_field()
    {
        factory(Stockpile::class)->create([
            'price'=>1792
        ]);

        $this->assertEquals(1.792, Stockpile::first()->price);
    }

    /** @test */
    public function stockpile_item_can_determine_its_type()
    {
        factory(Product::class)->create(['ataco_code'=>10001, 'code'=>'HR10001']);
        factory(Product::class)->create(['ataco_code'=>10002, 'code'=>'AL10002', 'brand'=>'Reklamni']);
        factory(Product::class)->create(['ataco_code'=>10003, 'code'=>'AL10003', 'brand'=>'Sample']);

        factory(Stockpile::class)->create(['code'=>'HR10001']);
        factory(Stockpile::class)->create(['code'=>'HR10007']);
        factory(Stockpile::class)->create(['code'=>'AL10002']);
        factory(Stockpile::class)->create(['code'=>'AL10003']);

        $stockPile = Stockpile::all();

        $this->assertEquals('product', $stockPile->shift()->type);
        $this->assertEquals('OTHER', $stockPile->shift()->type);
        $this->assertEquals('REKLAMNI', $stockPile->shift()->type);
        $this->assertEquals('SAMPLE', $stockPile->shift()->type);
    }


    /** @test */
    public function stockpile_items_can_be_created_from_text()
    {
        $text =
"code	title	amount	exp_date	price	total	truck	date	comment	type
AL2012-F-U	HiPP 1 Combiotic Infant Milk (p)	560	19.05.2019	3.58	2004.8	7.2018	12.04.2018
AL2013-01	HiPP 1 Combiotic Infant Milk (p)	900	28.05.2019	8.11	7299	7.2018	12.04.2018
AL2016-02-U	HiPP 1 Organic Infant Milk (p)	980	25.08.2019	3.15	3087	7.2018	12.04.2018		";

        $stockpile = Stockpile::getFromText($text);

        Stockpile::saveEntries($stockpile);

        $this->assertEquals(3, Stockpile::count());
    }
}
