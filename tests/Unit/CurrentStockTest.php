<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Stock;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Carbon\Carbon;
use App\Product;

class CurrentStockTest extends TestCase
{
    use DatabaseMigrations;
    /** @test */
    public function create_one_stock()
    {
        $this->disableExceptionHandling();
        factory(Product::class)->create(['ataco_code'=>10001]);
        factory(Stock::class)->create([
            'ataco_code'=>10001,
            'code'=>'HR10001',
            'amount'=>'100',
            'type'=>'Stock',
            'date'=>'2018-01-01',
    ]);
        $this->assertEquals(1, Stock::count());
        $this->assertEquals(10001, Stock::first()->ataco_code);
        // $this->assertEquals('HR10001', Stock::first()->code);
        $this->assertEquals(100, Stock::first()->amount);
        $this->assertEquals('Stock', Stock::first()->type);
        $this->assertEquals(Carbon::parse('2018-01-01'), Stock::first()->date);
    }

    /** @test */
    public function stock_has_formatted_date_field()
    {
        $this->disableExceptionHandling();
        factory(Stock::class)->create([
            'date'=>'2018-01-01',
        ]);

        $this->assertEquals(Carbon::parse('2018-01-01'), Stock::first()->date);
    }

    /** @test */
    public function stock_has_precalculated_month_and_year_fields()
    {
        $this->disableExceptionHandling();

        factory(Product::class)->create(['ataco_code'=>10001, 'code'=>'HR10001']);
        factory(Stock::class)->create([
            'ataco_code'=>10001,
            'date'=>'2018-05-01',
        ]);

        $this->assertEquals(2018, Stock::first()->year);
        $this->assertEquals(5, Stock::first()->month);
    }
}
