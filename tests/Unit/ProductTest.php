<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Product;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Stock_Sale;
use Illuminate\Support\Facades\Config;

class ProductTest extends TestCase
{
    use DatabaseMigrations;
    /** @test */
    public function products_can_be_created()
    {
        $product = factory(Product::class, 10)->create();

        $this->assertTrue(Product::count() == 10);
    }
    /** @test */
    public function product_strictly_defined_fields()
    {
        $product = factory(Product::class)->create([
            'code' =>       1050,
            'category' =>   'mlijeko',
            'name_eng' =>   'Name ENG',
            'name_ba' =>    'Name BA',
            'brand' =>      'HiPP',
            'country' =>    'BA',
            'comment' =>    'Comment',
            'ataco_code' => '100050',
            'em_no' =>      "EM_No",
            'status' =>     'active',
            'price' =>      1700,
            'type' =>       'gratis',
            'stranica' =>       1,
        ]);
        $this->assertTrue(Product::count() == 1);
        $this->assertEquals(1050, $product->code);
        $this->assertEquals('mlijeko', $product->category);
        $this->assertEquals('Name ENG', $product->name_eng);
        $this->assertEquals('Name BA', $product->name_ba);
        $this->assertEquals('HiPP', $product->brand);
        $this->assertEquals('BA', $product->country);
        $this->assertEquals('Comment', $product->comment);
        $this->assertEquals('100050', $product->ataco_code);
        $this->assertEquals("EM_No", $product->em_no);
        $this->assertEquals('active', $product->status);
        $this->assertEquals(1700, $product->price);
        $this->assertEquals('gratis', $product->type);
        $this->assertEquals(1, $product->stranica);
    }

    /** @test */
    public function product_code_is_unique()
    {
        factory(Product::class)->create(['code'=>'1111']);
        $this->assertEquals(Product::count(), 1);

        $this->expectException(\Exception::class);
        factory(Product::class)->create(['code'=>'1111']);
    }

    /** @test */
    public function product_ataco_code_is_unique()
    {
        factory(Product::class)->create(['ataco_code'=>'1111']);
        $this->assertEquals(Product::count(), 1);

        try {
            factory(Product::class)->create(['ataco_code'=>'1111']);
        } catch (\Exception $e) {
            return;
        }
        $this->fail('Product ataco_code must be unique');
    }

    /** @test */
    public function Product_has_unique_base_code_and_code_version_field()
    {
        $this->withoutExceptionHandling();

        factory(Product::class)->create(['code'=>'AL2016-01-U-F']);
        factory(Product::class)->create(['code'=>'AL2019']);
        $this->assertEquals(Product::count(), 2);

        $products = Product::all();

        $this->assertEquals('AL2016', $products->first()->code);
        $this->assertEquals('01-U-F', $products->first()->code_version);
        // dd($products->pop());
        $secondProduct = $products->pop();
        $this->assertEquals('AL2019', $secondProduct->code);
        $this->assertEquals(null, $secondProduct->code_version);
    }


    // TODO: fix if stocksale relatoin to  product is restored
    /** @test */
    // public function product_has_one_or_more_stock_sales_entries()
    // {
    //     $product = factory(Product::class)->create(['ataco_code'=>10001, 'code'=>3001]);
    //     $stock_sale = factory(Stock_Sale::class)->create(['ataco_code'=>10001, 'product_id'=>$product->id]);

    //     $this->assertEquals(1, Product::count());
    //     $this->assertEquals(1, Stock_Sale::count());


    //     $this->assertEquals(1, $product->stocks_sales->count());
    //     $this->assertEquals(10001, $product->stocks_sales->first()->ataco_code);
    // }

    /** @test */
    public function product_can_be_created_from_text()
    {
        $this->withoutExceptionHandling();
        $text ="ataco_code	code	name_eng	category
1110001	AL0002	Test Product	Mlijeko
1120001	HiPP0002	HiPP test	Mlijeko";

        $products = Product::all();
        $this->assertEquals(0, $products->count());

        Product::addProductsFromText($text);

        $products = Product::all();

        $this->assertEquals(2, $products->count());

        $p = $products->shift();
        $this->assertEquals("AL0002", $p->code);
        $this->assertEquals(1110001, $p->ataco_code);
        $this->assertEquals("Test Product", $p->name_eng);
        $this->assertEquals("Mlijeko", $p->category);

        $p = $products->shift();
        $this->assertEquals("HiPP0002", $p->code);
        $this->assertEquals(1120001, $p->ataco_code);
        $this->assertEquals("HiPP test", $p->name_eng);
        $this->assertEquals("Mlijeko", $p->category);
    }

    /** @test */
    public function product_can_change_web_status()
    {
        factory(Product::class)->create();
        $product = Product::first();
        $this->assertEquals(1, Product::count());
        $this->assertEquals(0, $product->fresh()->stranica);

        $product->stranica();
        $this->assertEquals(1, $product->stranica);
        $product->stranica();
        $this->assertEquals(0, $product->stranica);
    }

    /** @test */
    public function products_use_db_transactions()
    {
        Config::set('DB_CONNECTION', 'mysql_test');
        $this->withoutExceptionHandling();
        $text ="ataco_code	code	name_eng	category
1110001	AL0002	Test Product	Mlijeko
1110001	HiPP0002	HiPP test	Mlijeko";

        // Product::addProductsFromText($text);

        try {
            Product::addProductsFromText($text);
        } catch (\Exception $e) {
            $this->assertEquals(0, Product::count());
        }
    }
}
