let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// mix.js('resources/assets/js/app.js', 'public/js');
// mix.combine([
//     // "public/front_end/assets/js/jquery.min.js",
//     // "public/front_end/assets/js/bootstrap.min.js",
//     // "public/front_end/assets/js/detect.js",
//     // "public/front_end/assets/js/fastclick.js",
//     // "public/front_end/assets/js/jquery.slimscroll.js",
//     // "public/front_end/assets/js/jquery.blockUI.js",
//     // "public/front_end/assets/js/waves.js",
//     // "public/front_end/assets/js/jquery.nicescroll.js",
//     // "public/front_end/assets/js/jquery.scrollTo.min.js",
//     // "public/front_end/assets/js/jquery.core.js",
//     // "public/front_end/assets/js/jquery.app.js",
//     "public/front_end/assets/js/*"
// ], 'public/js/app.js')
//    .combine([
//     // "public/front_end/assets/css/bootstrap.min.css",
//     // "public/front_end/assets/css/core.css",
//     // "public/front_end/assets/css/components.css",
//     // "public/front_end/assets/css/icons.css",
//     // "public/front_end/assets/css/pages.css",
//     // "public/front_end/assets/css/menu.css",
//     // "public/front_end/assets/css/responsive.css",
//     "public/front_end/assets/css/*",
//    ], 'public/css/app.css');
   mix.browserSync({
    // host: '192.168.10.10',
    proxy: 'reporting.test',
    open: false,
    files: [
        'app/**/*.php',
        'resources/views/**/*.php',
        'public/js/**/*.js',
        'public/css/**/*.css'
    ],
    watchOptions: {
        usePolling: true,
        interval: 500
    }
});
